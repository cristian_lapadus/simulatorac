﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic.PowerPacks;
using Controller;

namespace View
{
    public partial class Form1 : Form, IForm1
    {
        Label[,] regs = new Label[2, 16];
        public int numberBase = 10;
        int timerInterval = 200;
        int previousBase = 10;
        public int nrOfDigits = 0;
        SimulatorController _controller;
        
        Color selected = Color.Blue;
        Color normal = Color.Black;
        Color updated = Color.Red;
        Color unInitialized = Color.Gray;
        public Form1()
        {
            InitializeComponent();
            
            ListViewItem lvi = new ListViewItem();

            listView1.Columns.Add("Registru");
            listView1.Columns[0].Width = 55;
            listView1.Columns.Add("Valoare");
            listView1.Columns[1].Width = 124;

            listView2.Columns.Add("Adresa");
            listView2.Columns[0].Width = 45;
            listView2.Columns.Add("Valoare");
            listView2.Columns[1].Width = 117;

            listView3.Columns.Add("Registru");
            listView3.Columns[0].Width = 55;
            listView3.Columns.Add("Valoare");
            listView3.Columns[1].Width = 124;

            listView4.Columns.Add("Registru");
            listView4.Columns[0].Width = 55;
            listView4.Columns.Add("Valoare");
            listView4.Columns[1].Width = 124;

            listView5.Columns.Add("Microadresa");
            listView5.Columns.Add("SBUS", 90, HorizontalAlignment.Center);
            listView5.Columns.Add("DBUS", 90, HorizontalAlignment.Center);
            listView5.Columns.Add("ALU", 90, HorizontalAlignment.Center);
            listView5.Columns.Add("RBUS", 90, HorizontalAlignment.Center);
            listView5.Columns.Add("MEM", 90, HorizontalAlignment.Center);
            listView5.Columns.Add("OTHER", 90, HorizontalAlignment.Center);
            listView5.Columns.Add("SALT", 180, HorizontalAlignment.Center);
            listView5.Columns.Add("INDEX", 90, HorizontalAlignment.Center);

            listView5.Items.Add("adr");
            listView5.Items[0].SubItems.Add("sbus");
            listView5.Items[0].SubItems.Add("dbus");
            listView5.Items[0].SubItems.Add("alu");
            listView5.Items[0].SubItems.Add("rbus");
            listView5.Items[0].SubItems.Add("mem");
            listView5.Items[0].SubItems.Add("other");
            listView5.Items[0].SubItems.Add("salt");
            listView5.Items[0].SubItems.Add("index");
            listView5.Items[0].UseItemStyleForSubItems = false;

            rectangleShape1.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            rectangleShape1.FillColor = Color.CornflowerBlue;
            rectangleShape2.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            rectangleShape2.FillColor = Color.CornflowerBlue;
            rectangleShape3.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            rectangleShape3.FillColor = Color.CornflowerBlue;
            rectangleShape4.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            rectangleShape4.FillColor = Color.CornflowerBlue;
            rectangleShape5.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            rectangleShape5.FillColor = Color.CornflowerBlue;

            for (int i = 0; i < 16; i++)
            {
                lvi = new ListViewItem("R" + i);
                lvi.SubItems.Add("0000000000000000");
                listView1.Items.Add(lvi);
            }

            lvi = new ListViewItem("ADR");
            lvi.SubItems.Add("0000000000000000");
            listView4.Items.Add(lvi);

            lvi = new ListViewItem("PC");
            lvi.SubItems.Add("0000000000000000");
            listView4.Items.Add(lvi);

            lvi = new ListViewItem("IVR");
            lvi.SubItems.Add("0000000000000000");
            listView4.Items.Add(lvi);

            lvi = new ListViewItem("MDR");
            lvi.SubItems.Add("0000000000000000");
            listView3.Items.Add(lvi);

            lvi = new ListViewItem("IR");
            lvi.SubItems.Add("0000000000000000");
            listView3.Items.Add(lvi);

            lvi = new ListViewItem("T");
            lvi.SubItems.Add("0000000000000000");
            listView3.Items.Add(lvi);

            lvi = new ListViewItem("SP");
            lvi.SubItems.Add("0000000000000000");
            listView3.Items.Add(lvi);

            lvi = new ListViewItem("FLAG");
            lvi.SubItems.Add("0000000000000000");
            listView3.Items.Add(lvi);

            listView6.Columns.Add("Microaddresa");
            listView6.Columns.Add("ETICHETA", 110, HorizontalAlignment.Center);
            listView6.Columns.Add("CODUL", 460);

            listView7.Columns.Add("Nr");
            listView7.Columns.Add(" ",700);
            


            _controller = new SimulatorController(this);

            ResetColor(unInitialized);

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            //alu
            Graphics g = panel1.CreateGraphics();
            Point[] points = new Point[7];
            points[0] = new Point(200, 400);
            points[1] = new Point(200, 450);
            points[2] = new Point(250, 475);
            points[3] = new Point(250, 430);
            points[4] = new Point(240, 425);
            points[5] = new Point(250, 420);
            points[6] = new Point(250, 375);
            Brush black = new SolidBrush(Color.Black);
            Pen bPen = new Pen(black, 2f);
            g.DrawPolygon(bPen, points);
            //alu
        }
        

        public void Alu(string s)
        {
            label27.Text = s;
            label27.ForeColor = updated;
        }

        public void PmRG(int index, int value)
        {
            lineShape2.BorderColor = updated;
            lineShape3.BorderColor = updated;
            lineShape4.BorderColor = updated;
            label8.ForeColor = updated;
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Index == index)
                {
                    item.ForeColor = updated;
                    item.SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
                }
            }
        }
        public void PdRGd(int index)
        {
            lineShape5.BorderColor = selected;
            lineShape6.BorderColor = selected;
            lineShape7.BorderColor = selected;
            label9.ForeColor = selected;
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Index == index)
                {
                    item.ForeColor = selected;
                    break;
                }
            }
            rectangleShape5.BorderColor = selected;
        }
        public void PdRGs(int index)
        {
            lineShape8.BorderColor = selected;
            lineShape9.BorderColor = selected;
            lineShape10.BorderColor = selected;
            label9.ForeColor = selected;
            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Index == index)
                {
                    item.ForeColor = selected;
                    break;
                }
            }
            rectangleShape4.BorderColor = selected;
        }
        public void PmIVR(int value)
        {
            listView4.Items[2].ForeColor = updated;
            listView4.Items[2].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label12.ForeColor = updated;
            lineShape11.BorderColor = updated;
            lineShape12.BorderColor = updated;
            lineShape13.BorderColor = updated;
        }
        public void PdIVRs()
        {
            lineShape35.BorderColor = selected;
            lineShape36.BorderColor = selected;
            lineShape37.BorderColor = selected;
            label20.ForeColor = selected;
            listView4.Items[2].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdIVRd()
        {
            lineShape26.BorderColor = selected;
            lineShape27.BorderColor = selected;
            lineShape28.BorderColor = selected;
            label20.ForeColor = selected;
            listView4.Items[2].ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void PmPC(int value)
        {
            listView4.Items[1].ForeColor = updated;
            listView4.Items[1].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label11.ForeColor = updated;
            lineShape14.BorderColor = updated;
            lineShape15.BorderColor = updated;
            lineShape16.BorderColor = updated;
        }
        public void PdPCs()
        {
            lineShape32.BorderColor = selected;
            lineShape33.BorderColor = selected;
            lineShape34.BorderColor = selected;
            label13.ForeColor = selected;
            listView4.Items[1].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdPCd()
        {
            lineShape23.BorderColor = selected;
            lineShape24.BorderColor = selected;
            lineShape25.BorderColor = selected;
            label13.ForeColor = selected;
            listView4.Items[1].ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void PmADR(int value)
        {
            listView4.Items[0].ForeColor = updated;
            listView4.Items[0].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label10.ForeColor = updated;
            lineShape17.BorderColor = updated;
            lineShape18.BorderColor = updated;
            lineShape19.BorderColor = updated;
        }
        public void PdADRs()
        {
            lineShape29.BorderColor = selected;
            lineShape30.BorderColor = selected;
            lineShape31.BorderColor = selected;
            lineShape120.BorderColor = selected;
            label19.ForeColor = selected;
            listView4.Items[0].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdADRd()
        {
            lineShape20.BorderColor = selected;
            lineShape21.BorderColor = selected;
            lineShape22.BorderColor = selected;
            label19.ForeColor = selected;
            listView4.Items[0].ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void PmMDR(int value)
        {
            listView3.Items[0].ForeColor = updated;
            listView3.Items[0].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label7.ForeColor = updated;
            lineShape79.BorderColor = updated;
            lineShape80.BorderColor = updated;
            lineShape81.BorderColor = updated;
            lineShape113.BorderColor = updated;
            lineShape114.BorderColor = updated;
            lineShape115.BorderColor = updated;
            rectangleShape12.BorderColor = updated;
        }
        public void PdMDRs()
        {
            lineShape45.BorderColor = selected;
            lineShape46.BorderColor = selected;
            lineShape47.BorderColor = selected;
            lineShape119.BorderColor = selected;
            label14.ForeColor = selected;
            listView3.Items[0].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdMDRd()
        {
            lineShape48.BorderColor = selected;
            lineShape49.BorderColor = selected;
            lineShape50.BorderColor = selected;
            label14.ForeColor = selected;
            listView3.Items[0].ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void PdIRs()
        {
            lineShape51.BorderColor = selected;
            lineShape55.BorderColor = selected;
            lineShape56.BorderColor = selected;
            label15.ForeColor = selected;
            listView3.Items[1].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdIRd()
        {
            lineShape52.BorderColor = selected;
            lineShape53.BorderColor = selected;
            lineShape54.BorderColor = selected;
            label15.ForeColor = selected;
            listView3.Items[1].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PmSP(int value)
        {
            listView3.Items[3].ForeColor = updated;
            listView3.Items[3].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label3.ForeColor = updated;
            lineShape85.BorderColor = updated;
            lineShape86.BorderColor = updated;
            lineShape87.BorderColor = updated;
        }
        public void PdSPs()
        {
            lineShape63.BorderColor = selected;
            lineShape67.BorderColor = selected;
            lineShape68.BorderColor = selected;
            label18.ForeColor = selected;
            listView3.Items[3].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdSPd()
        {
            lineShape64.BorderColor = selected;
            lineShape65.BorderColor = selected;
            lineShape66.BorderColor = selected;
            label18.ForeColor = selected;
            listView3.Items[3].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PmT(int value)
        {
            listView3.Items[2].ForeColor = updated;
            listView3.Items[2].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label2.ForeColor = updated;
            lineShape82.BorderColor = updated;
            lineShape83.BorderColor = updated;
            lineShape84.BorderColor = updated;
        }
        public void PdTs()
        {
            lineShape61.BorderColor = selected;
            lineShape62.BorderColor = selected;
            lineShape57.BorderColor = selected;
            label16.ForeColor = selected;
            listView3.Items[2].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdTd()
        {
            lineShape58.BorderColor = selected;
            lineShape59.BorderColor = selected;
            lineShape60.BorderColor = selected;
            label16.ForeColor = selected;
            listView3.Items[2].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PmFLAG(int value)
        {
            listView3.Items[4].ForeColor = updated;
            listView3.Items[4].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label1.ForeColor = updated;
            lineShape88.BorderColor = updated;
            lineShape89.BorderColor = updated;
            lineShape90.BorderColor = updated;
            lineShape116.BorderColor = updated;
            lineShape117.BorderColor = updated;
            lineShape118.BorderColor = updated;
            rectangleShape11.BorderColor = updated;
        }
        public void PdFLAGs()
        {
            lineShape69.BorderColor = selected;
            lineShape73.BorderColor = selected;
            lineShape74.BorderColor = selected;
            label17.ForeColor = selected;
            listView3.Items[4].ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdFLAGd()
        {
            lineShape70.BorderColor = selected;
            lineShape71.BorderColor = selected;
            lineShape72.BorderColor = selected;
            label17.ForeColor = selected;
            listView3.Items[4].ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void Pd0s()
        {
            lineShape121.BorderColor = selected;
            lineShape122.BorderColor = selected;
            lineShape124.BorderColor = selected;
            label24.ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void Pd0d()
        {
            lineShape123.BorderColor = selected;
            lineShape126.BorderColor = selected;
            lineShape125.BorderColor = selected;
            label24.ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void Pd1s()
        {
            lineShape127.BorderColor = selected;
            lineShape128.BorderColor = selected;
            lineShape129.BorderColor = selected;
            label25.ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void Pd1d()
        {
            lineShape130.BorderColor = selected;
            lineShape131.BorderColor = selected;
            lineShape132.BorderColor = selected;
            label25.ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void PdMinus1s()
        {
            lineShape133.BorderColor = selected;
            lineShape134.BorderColor = selected;
            lineShape135.BorderColor = selected;
            label26.ForeColor = selected;
            rectangleShape4.BorderColor = selected;
        }
        public void PdMinus1d()
        {
            lineShape136.BorderColor = selected;
            lineShape137.BorderColor = selected;
            lineShape138.BorderColor = selected;
            label26.ForeColor = selected;
            rectangleShape5.BorderColor = selected;
        }
        public void PdCond(int value)
        {
            listView3.Items[4].ForeColor = updated;
            listView3.Items[4].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label6.ForeColor = updated;
            lineShape100.BorderColor = updated;
            lineShape101.BorderColor = updated;
            lineShape102.BorderColor = updated;
            lineShape103.BorderColor = updated;
            lineShape104.BorderColor = updated;
            lineShape105.BorderColor = updated;
            lineShape116.BorderColor = updated;
            lineShape117.BorderColor = updated;
            lineShape118.BorderColor = updated;
            rectangleShape11.BorderColor = updated;
        }
        public void MemRead(int address, int value)
        {
            listView2.Items[address].ForeColor = selected;

            listView3.Items[0].ForeColor = updated;
            listView3.Items[0].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label23.ForeColor = updated;

            lineShape110.BorderColor = updated;
            lineShape111.BorderColor = updated;
            lineShape112.BorderColor = updated;
            lineShape113.BorderColor = updated;
            lineShape114.BorderColor = updated;
            lineShape115.BorderColor = updated;
            lineShape106.BorderColor = updated;
            lineShape107.BorderColor = updated;
            lineShape108.BorderColor = updated;
            lineShape109.BorderColor = updated;
            rectangleShape12.BorderColor = updated;
        }
        public void IFCH(int address, int value)
        {
            listView2.Items[address].ForeColor = selected;

            listView3.Items[1].ForeColor = updated;
            listView3.Items[1].SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
            label23.ForeColor = updated;

            lineShape110.BorderColor = updated;
            lineShape111.BorderColor = updated;
            lineShape112.BorderColor = updated;

            lineShape106.BorderColor = updated;
            lineShape1.BorderColor = updated;
            lineShape42.BorderColor = updated;
            lineShape43.BorderColor = updated;
            lineShape44.BorderColor = updated;

            lineShape29.BorderColor = selected;
            lineShape41.BorderColor = selected;
            lineShape40.BorderColor = selected;
            lineShape39.BorderColor = selected;
            lineShape38.BorderColor = selected;
        }
        public void MemoryWrite(int index, int value)
        {
            lineShape47.BorderColor = updated;
            lineShape75.BorderColor = updated;
            lineShape76.BorderColor = updated;
            lineShape77.BorderColor = updated;
            lineShape78.BorderColor = updated;
            label22.ForeColor = updated;
            label21.ForeColor = updated;

            lineShape29.BorderColor = selected;
            lineShape41.BorderColor = selected;
            lineShape40.BorderColor = selected;
            lineShape39.BorderColor = selected;
            lineShape38.BorderColor = selected;
            foreach (ListViewItem item in listView2.Items)
            {
                if (item.Index == index)
                {
                    item.ForeColor = updated;
                    item.SubItems[1].Text = Convert.ToString(value, numberBase).PadLeft(nrOfDigits, '0');
                    listView2.EnsureVisible(index);
                    break;
                }
            }
        }
        public void Plus2PC()
        {
            listView4.Items[1].ForeColor = updated;
            listView4.Items[1].SubItems[1].Text = Convert.ToString(_controller.GetAdr_Pc_Ivr()[1], numberBase).PadLeft(nrOfDigits, '0');
            label11.ForeColor = updated;
        }
        public void Plus2SP()
        {
            listView3.Items[3].ForeColor = updated;
            listView3.Items[3].SubItems[1].Text = Convert.ToString(_controller.GetMdr_Flag()[3], numberBase).PadLeft(nrOfDigits, '0');
            label11.ForeColor = updated;
        }
        public void Minus2SP()
        {
            listView3.Items[3].ForeColor = updated;
            listView3.Items[3].SubItems[1].Text = Convert.ToString(_controller.GetMdr_Flag()[3], numberBase).PadLeft(nrOfDigits, '0');
            label11.ForeColor = updated;
        }
        public void OtherFlags()
        {
            listView3.Items[4].ForeColor = updated;
            listView3.Items[4].SubItems[1].Text = Convert.ToString(_controller.GetMdr_Flag()[4], numberBase).PadLeft(nrOfDigits, '0');
            label11.ForeColor = updated;
        }
        public void ResetColor()
        {
            foreach (var item in shapeContainer1.Shapes)
            {
                ((Shape)item).BorderColor = normal;
            }
            foreach (var item in panel1.Controls)
            {
                if (item.GetType() == typeof(Label))
                    ((Label)item).ForeColor = normal;
                if (item.GetType() == typeof(ListView))
                    foreach (ListViewItem v in ((ListView)item).Items)
                    {
                        v.ForeColor = normal;
                    }
            }
        }
        private void ResetColor(Color c)
        {
            foreach (var item in shapeContainer1.Shapes)
            {
                ((Shape)item).BorderColor = c;
            }
            foreach (var item in panel1.Controls)
            {
                if (item.GetType() == typeof(Label))
                    ((Label)item).ForeColor = c;
                if (item.GetType() == typeof(ListView))
                    foreach (ListViewItem v in ((ListView)item).Items)
                    {
                        v.ForeColor = c;
                    }
            }
        }

        int previousLineMI=0;
        int previousLineI = 0;
        private void HighlightLine(int pc)
        {
            int lineNumberToSelect = 0;
            if (_controller.LinesPC.Contains(pc))// || _controller.LinesPC.Contains(pc + 1) || _controller.LinesPC.Contains(pc+2))
            {
                lineNumberToSelect = _controller.LinesPC.IndexOf(pc);
                listView7.Items[previousLineI].BackColor = SystemColors.Window;
                listView7.Items[lineNumberToSelect].BackColor = Color.Yellow;
                listView7.EnsureVisible(lineNumberToSelect);
                previousLineI = lineNumberToSelect;
            }
            else
                if (_controller.LinesPC.Contains(pc + 1))// || _controller.LinesPC.Contains(pc + 1) || _controller.LinesPC.Contains(pc+2))
                {
                    lineNumberToSelect = _controller.LinesPC.IndexOf(pc + 1);
                    listView7.Items[previousLineI].BackColor = SystemColors.Window;
                    listView7.Items[lineNumberToSelect].BackColor = Color.Yellow;
                    listView7.EnsureVisible(lineNumberToSelect);
                    previousLineI = lineNumberToSelect;
                }
                else if (_controller.LinesPC.Contains(pc + 2))// || _controller.LinesPC.Contains(pc + 1) || _controller.LinesPC.Contains(pc+2))
                {
                    lineNumberToSelect = _controller.LinesPC.IndexOf(pc + 2);
                    listView7.Items[previousLineI].BackColor = SystemColors.Window;
                    listView7.Items[lineNumberToSelect].BackColor = Color.Yellow;
                    listView7.EnsureVisible(lineNumberToSelect);
                    previousLineI = lineNumberToSelect;
                }
        }
        private void stepMC_Click(object sender, EventArgs e)
        {
            int res = -1;
            try
            {
                int pc = _controller.GetPC();
                HighlightLine(pc);

                res = _controller.StepMicroComanda();

                int lineNumberToSelect = _controller.GetMar();
                listView6.Items[previousLineMI].BackColor = SystemColors.Window;
                listView6.Items[lineNumberToSelect].BackColor = Color.Yellow;
                listView6.EnsureVisible(lineNumberToSelect);
                previousLineMI = lineNumberToSelect;
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Final",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                res = 0;
                timer1.Stop();
            }
            if (res==-1)
            {
                MessageBox.Show("Incarcati mai intai codul si microcodul!", "Eroare",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                timer1.Stop();
            }
        }

        private void stepMI_Click(object sender, EventArgs e)
        {
            bool res = false;
            try
            {
                int pc = _controller.GetPC();
                HighlightLine(pc);

                res = _controller.StepMicroInstructiune();

                int lineNumberToSelect = _controller.GetMar();
                listView6.Items[previousLineMI].BackColor = SystemColors.Window;
                listView6.Items[lineNumberToSelect].BackColor = Color.Yellow;
                listView6.EnsureVisible(lineNumberToSelect);
                previousLineMI = lineNumberToSelect;
            }
            catch (Exception ex)
            {
                if (timer1.Enabled)
                    timer1.Enabled = false;
                
                //MessageBox.Show(ex.Message, "Final",
                //MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                
            
            }
            if (!res)
            {
                MessageBox.Show("Incarcati mai intai codul si microcodul!", "Eroare",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                timer1.Stop();
            }
        }

        private void stepI_Click(object sender, EventArgs e)
        {
            bool res = false;
            try
            {

                int pc = _controller.GetPC();
                HighlightLine(pc);
                res = _controller.StepInstructiune();
                pc = _controller.GetPC();
                HighlightLine(pc);

                int lineNumberToSelect = _controller.GetMar();
                listView6.Items[previousLineMI].BackColor = SystemColors.Window;
                listView6.Items[lineNumberToSelect].BackColor = Color.Yellow;
                listView6.EnsureVisible(lineNumberToSelect);
                previousLineMI = lineNumberToSelect;

               
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Final",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                res = true;
            }
            if (!res)
            {
                MessageBox.Show("Incarcati mai intai codul si microcodul!", "Eroare",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void run_Click(object sender, EventArgs e)
        { 
             timer1.Start();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text files |*.txt";
            openFileDialog1.FileName = "";
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.InitialDirectory = @"D:\Calc\AC\Proiect\";
            openFileDialog1.ShowDialog();

        }

        private void incarcaMicrocodulToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text files |*.txt";
            openFileDialog1.FileName = "";
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.InitialDirectory = @"D:\Calc\AC\Proiect\";
            openFileDialog1.ShowDialog();
        }
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            _controller.InitMicroasamblor(openFileDialog1.FileName);
            ShowMicrocode(openFileDialog1.FileName);
            if (_controller.IsReady())
            {
                _controller.InitSimulator();
                UpdateMem();
                UpdateMDR_FLAG();
                UpdateADR_IVR();
                UpdateRG();
                ResetColor();
            }
        }

        private void ShowMicrocode(String fName)
        {
            int i = 0;
            using (StreamReader sr = new StreamReader(fName))
            {

                while (!sr.EndOfStream)
                {
                    String line = sr.ReadLine();
                    line = line.Trim();
                    listView6.Items.Add(i.ToString());
                    if (line.Contains(":"))
                    {
                        String[] s = line.Split(':');
                        if (s != null)
                        {
                            listView6.Items[i].SubItems.Add(s[0]);
                            listView6.Items[i].SubItems.Add(s[1]);
                        }
                    }
                    else
                    {
                        listView6.Items[i].SubItems.Add(" ");
                        listView6.Items[i].SubItems.Add(line);                        
                    }
                    i++;
                }
            }
        }
        private void ShowCode(String fName)
        {
            int i = 0;
            using (StreamReader sr = new StreamReader(fName))
            {

                while (!sr.EndOfStream)
                {
                    String line = sr.ReadLine();

                    listView7.Items.Add(i.ToString());
                   line= line.Replace('\t', ' ');
                   listView7.Items[i].SubItems.Add(line);

                    i++;
                    
                    }
            }
        }
        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            _controller.InitAsamblor(openFileDialog2.FileName);
            ShowCode(openFileDialog2.FileName);
            if (_controller.IsReady())
            {
                _controller.InitSimulator();

                UpdateMem();
                UpdateMDR_FLAG();
                UpdateADR_IVR();
                UpdateRG();
                ResetColor();
            }
        }
        public void UpdateRG()
        {
            var values = _controller.GetRG();
            for (int i = 0; i < values.Length; i++)
            {
                if (listView1.Items[i].SubItems[1].Text != Convert.ToString((int)values[i], numberBase).PadLeft(nrOfDigits, '0'))
                {
                    listView1.Items[i].SubItems[1].Text = Convert.ToString((int)values[i], numberBase).PadLeft(nrOfDigits, '0');

                    listView1.Items[i].ForeColor = updated;
                }
            }
        }
        public void UpdateADR_IVR()
        {
            var values = _controller.GetAdr_Pc_Ivr();
            for (int i = 0; i < values.Length; i++)
            {
                if (listView4.Items[i].SubItems[1].Text != Convert.ToString(values[i], numberBase).PadLeft(nrOfDigits, '0'))
                {
                    listView4.Items[i].SubItems[1].Text = Convert.ToString(values[i], numberBase).PadLeft(nrOfDigits, '0');

                    listView4.Items[i].ForeColor = updated;
                }
            }
        }
        public void UpdateMDR_FLAG()
        {
            var values = _controller.GetMdr_Flag();
            for (int i = 0; i < values.Length; i++)
            {
                if (listView3.Items[i].SubItems[1].Text != Convert.ToString(values[i], numberBase).PadLeft(nrOfDigits, '0'))
                {
                    listView3.Items[i].SubItems[1].Text = Convert.ToString(values[i], numberBase).PadLeft(nrOfDigits, '0');

                    listView3.Items[i].ForeColor = updated;
                }
            }
        }
        public void UpdateMem()
        {
            var values = _controller.GetMemory();
            if (listView2.Items.Count == 0)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    var lvi = new ListViewItem(i.ToString());
                    lvi.SubItems.Add(Convert.ToString(values[i], numberBase));
                    listView2.Items.Add(lvi);
                }
                listView2.Items[_controller.GetPC()].EnsureVisible();
            }
            else
            {
                for (int i = 0; i < values.Length; i++)
                {
                    if (listView2.Items[i].SubItems[1].Text != Convert.ToString(values[i], numberBase).PadLeft(nrOfDigits, '0'))
                    {
                        listView2.Items[i].SubItems[1].Text = Convert.ToString(values[i], numberBase).PadLeft(nrOfDigits, '0');

                        listView2.Items[i].ForeColor = updated;

                        listView2.Items[i].EnsureVisible();
                    }
                }
            }
        }
        public void MicroInstrText(string[] text, int n)
        {
            for (int i = 0; i < 9; i++)
            {
                listView5.Items[0].SubItems[i].Text = text[i];
                listView5.Items[0].SubItems[i].ForeColor = normal;
                if (i == n + 1)
                    listView5.Items[0].SubItems[i].ForeColor = updated;
            }
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            openFileDialog2.Filter = "Asm files |*.asm";
            openFileDialog2.FileName = "";
            openFileDialog2.CheckPathExists = true;
            openFileDialog2.InitialDirectory = @"D:\Calc\AC\Proiect\";
            openFileDialog2.ShowDialog();
        }

        private void incarcaProgramulToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog2.Filter = "Asm files |*.asm";
            openFileDialog2.FileName = "";
            openFileDialog2.CheckPathExists = true;
            openFileDialog2.InitialDirectory = @"D:\Calc\AC\Proiect\";
            openFileDialog2.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void afisareBaza10ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            previousBase = numberBase;
            numberBase = 10;
            nrOfDigits = 0;
            updateNumbers();
        }

        private void afisareBaza2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            previousBase = numberBase;
            numberBase = 2;
            nrOfDigits = 16;
            updateNumbers();
        }

        private void afisareBaza16ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            previousBase = numberBase;
            numberBase = 16;
            nrOfDigits = 4;
            updateNumbers();
        }
        private void updateNumbers()
        {
            foreach (ListViewItem item in listView1.Items)
            {

                int res;
                String text = item.SubItems[1].Text;
                res = Convert.ToInt32(text, previousBase);

                item.SubItems[1].Text = Convert.ToString(res, numberBase).PadLeft(nrOfDigits, '0');

            }
            foreach (ListViewItem item in listView2.Items)
            {
                int res;
                String text = item.SubItems[1].Text;
                res = Convert.ToInt32(text, previousBase);
                item.SubItems[1].Text = Convert.ToString(res, numberBase).PadLeft(nrOfDigits, '0');
            }
            foreach (ListViewItem item in listView3.Items)
            {
                int res;
                String text = item.SubItems[1].Text;
                res = Convert.ToInt32(text, previousBase);
                item.SubItems[1].Text = Convert.ToString(res, numberBase).PadLeft(nrOfDigits, '0');
            }
            foreach (ListViewItem item in listView4.Items)
            {
                int res;
                String text = item.SubItems[1].Text;
                res = Convert.ToInt32(text, previousBase);
                item.SubItems[1].Text = Convert.ToString(res, numberBase).PadLeft(nrOfDigits, '0');
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            _controller.Reset();
            timer1.Stop();
        }

        private void salveazaFisierulBinarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_controller.IsReady())
            {
                saveFileDialog1.Filter = "Binary File|*.o|Binary File|*.obj|Binary File|*.bin";
                saveFileDialog1.Title = "Save Compiled Code";
                saveFileDialog1.ShowDialog();
                if (saveFileDialog1.FileName != "")
                {
                    System.IO.FileStream fs =
                    (System.IO.FileStream)saveFileDialog1.OpenFile();

                    byte[] b = _controller.GetCompiledBytes();
                    fs.Write(b, 0, b.Length);

                    fs.Close();
                }

            }
            else
            {
                MessageBox.Show("Incarcati mai intai codul si microcodul!", "Eroare",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.stepMI_Click(sender, null);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            bool res = false;
            timer1.Stop();
            try
            {
                res = _controller.Run();
                int lineNumberToSelect = _controller.GetMar();
                listView6.Items[previousLineMI].BackColor = SystemColors.Window;
                listView6.Items[lineNumberToSelect].BackColor = Color.Yellow;
                listView6.EnsureVisible(lineNumberToSelect);
                previousLineMI = lineNumberToSelect;

                UpdateRG();
                UpdateADR_IVR();
                UpdateMDR_FLAG();
                UpdateMem();
            }
            catch (Exception ex)
            {
                
                MessageBox.Show(ex.Message, "Final",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                res = true;
            }
            if (!res)
            {
                MessageBox.Show("Incarcati mai intai codul si microcodul!", "Eroare",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
