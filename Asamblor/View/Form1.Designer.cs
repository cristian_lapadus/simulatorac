﻿namespace View
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listView5 = new System.Windows.Forms.ListView();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listView3 = new System.Windows.Forms.ListView();
            this.listView4 = new System.Windows.Forms.ListView();
            this.listView2 = new System.Windows.Forms.ListView();
            this.listView1 = new System.Windows.Forms.ListView();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape138 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape137 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape136 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape135 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape134 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape133 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape132 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape131 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape130 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape129 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape128 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape127 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape126 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape125 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape124 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape123 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape122 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape121 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape15 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape14 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape13 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape120 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape119 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape118 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape117 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape116 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape115 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape114 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape113 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape112 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape111 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape110 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape109 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape108 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape107 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape12 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape106 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape105 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape104 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape103 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape102 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape101 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape100 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape11 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.lineShape99 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape98 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape97 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape96 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape95 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape94 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape93 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape92 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape91 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape44 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape43 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape42 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape90 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape89 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape88 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape87 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape86 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape85 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape84 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape83 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape82 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape81 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape80 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape79 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape78 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape77 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape76 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape75 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape74 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape73 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape72 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape71 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape70 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape69 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape68 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape67 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape66 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape65 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape64 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape63 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape62 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape61 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape60 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape59 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape58 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape57 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape56 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape55 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape54 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape53 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape52 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape51 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape50 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape49 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape48 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape47 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape46 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape45 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape41 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape40 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape39 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape38 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape37 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape36 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape35 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape34 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape33 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape32 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape31 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape30 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape29 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape28 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape27 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape26 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape25 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape24 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape23 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape22 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape21 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape20 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape19 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape18 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape17 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape16 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape15 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape14 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape13 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape12 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape11 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape10 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.rectangleShape10 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape9 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape8 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape7 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape6 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape4 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape3 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape1 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listView7 = new System.Windows.Forms.ListView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listView6 = new System.Windows.Forms.ListView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.stepMC = new System.Windows.Forms.ToolStripButton();
            this.stepMI = new System.Windows.Forms.ToolStripButton();
            this.stepI = new System.Windows.Forms.ToolStripButton();
            this.run = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fisierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaMicrocodulToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incarcaProgramulToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salveazaFisierulBinarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optiuniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afisareBaza10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afisareBaza2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afisareBaza16ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog1";
            this.openFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl1.Location = new System.Drawing.Point(0, 52);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(996, 631);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(988, 605);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Arhitectura";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.listView5);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.listView3);
            this.panel1.Controls.Add(this.listView4);
            this.panel1.Controls.Add(this.listView2);
            this.panel1.Controls.Add(this.listView1);
            this.panel1.Controls.Add(this.shapeContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(982, 599);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // listView5
            // 
            this.listView5.Location = new System.Drawing.Point(21, -3);
            this.listView5.Name = "listView5";
            this.listView5.Size = new System.Drawing.Size(940, 55);
            this.listView5.TabIndex = 34;
            this.listView5.UseCompatibleStateImageBehavior = false;
            this.listView5.View = System.Windows.Forms.View.Details;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(205, 420);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(38, 13);
            this.label27.TabIndex = 33;
            this.label27.Text = "NONE";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.Location = new System.Drawing.Point(370, 461);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 22);
            this.label26.TabIndex = 32;
            this.label26.Text = "-1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.Location = new System.Drawing.Point(370, 410);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(21, 22);
            this.label25.TabIndex = 31;
            this.label25.Text = "1";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.Location = new System.Drawing.Point(370, 361);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(21, 22);
            this.label24.TabIndex = 30;
            this.label24.Text = "0";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.Location = new System.Drawing.Point(85, 159);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 15);
            this.label23.TabIndex = 28;
            this.label23.Text = "Data OUT";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.Location = new System.Drawing.Point(347, 146);
            this.label22.Margin = new System.Windows.Forms.Padding(0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 15);
            this.label22.TabIndex = 27;
            this.label22.Text = "Data IN";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.Location = new System.Drawing.Point(347, 117);
            this.label21.Margin = new System.Windows.Forms.Padding(0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 15);
            this.label21.TabIndex = 26;
            this.label21.Text = "ADR";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(484, 163);
            this.label20.Margin = new System.Windows.Forms.Padding(0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 12);
            this.label20.TabIndex = 25;
            this.label20.Text = "PdIVR";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(519, 125);
            this.label19.Margin = new System.Windows.Forms.Padding(0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 15);
            this.label19.TabIndex = 24;
            this.label19.Text = "PdADR";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(483, 312);
            this.label18.Margin = new System.Windows.Forms.Padding(0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 15);
            this.label18.TabIndex = 23;
            this.label18.Text = "PdSP";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(479, 330);
            this.label17.Margin = new System.Windows.Forms.Padding(0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 15);
            this.label17.TabIndex = 22;
            this.label17.Text = "PdFLAG";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(483, 295);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 15);
            this.label16.TabIndex = 21;
            this.label16.Text = "PdT";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(482, 277);
            this.label15.Margin = new System.Windows.Forms.Padding(0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 15);
            this.label15.TabIndex = 20;
            this.label15.Text = "PdIR";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(479, 259);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 12);
            this.label14.TabIndex = 19;
            this.label14.Text = "PdMDR";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(487, 146);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 18;
            this.label13.Text = "PdPC";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(894, 172);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "PmIVR";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(894, 152);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 12);
            this.label11.TabIndex = 16;
            this.label11.Text = "PmPC";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(894, 136);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 12);
            this.label10.TabIndex = 15;
            this.label10.Text = "PmADR";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(616, 489);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 14);
            this.label9.TabIndex = 14;
            this.label9.Text = "PdRG";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(901, 372);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 14);
            this.label8.TabIndex = 13;
            this.label8.Text = "PmRG";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(42, 300);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "PmSP";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(33, 249);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 14);
            this.label7.TabIndex = 11;
            this.label7.Text = "PmMDR";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(133, 379);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "PdCond";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(96, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 39);
            this.label5.TabIndex = 9;
            this.label5.Text = "M\r\nU\r\nX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(96, 324);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 39);
            this.label4.TabIndex = 8;
            this.label4.Text = "M\r\nU\r\nX";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(44, 283);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "PmT";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(40, 331);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "PmFLAG";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listView3
            // 
            this.listView3.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView3.Location = new System.Drawing.Point(132, 230);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(183, 116);
            this.listView3.TabIndex = 3;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            // 
            // listView4
            // 
            this.listView4.Location = new System.Drawing.Point(680, 113);
            this.listView4.Name = "listView4";
            this.listView4.Size = new System.Drawing.Size(183, 80);
            this.listView4.TabIndex = 4;
            this.listView4.UseCompatibleStateImageBehavior = false;
            this.listView4.View = System.Windows.Forms.View.Details;
            // 
            // listView2
            // 
            this.listView2.Location = new System.Drawing.Point(132, 51);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(183, 156);
            this.listView2.TabIndex = 2;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(680, 219);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(183, 304);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape138,
            this.lineShape137,
            this.lineShape136,
            this.lineShape135,
            this.lineShape134,
            this.lineShape133,
            this.lineShape132,
            this.lineShape131,
            this.lineShape130,
            this.lineShape129,
            this.lineShape128,
            this.lineShape127,
            this.lineShape126,
            this.lineShape125,
            this.lineShape124,
            this.lineShape123,
            this.lineShape122,
            this.lineShape121,
            this.rectangleShape15,
            this.rectangleShape14,
            this.rectangleShape13,
            this.lineShape120,
            this.lineShape119,
            this.lineShape1,
            this.lineShape118,
            this.lineShape117,
            this.lineShape116,
            this.lineShape115,
            this.lineShape114,
            this.lineShape113,
            this.lineShape112,
            this.lineShape111,
            this.lineShape110,
            this.lineShape109,
            this.lineShape108,
            this.lineShape107,
            this.rectangleShape12,
            this.lineShape106,
            this.lineShape105,
            this.lineShape104,
            this.lineShape103,
            this.lineShape102,
            this.lineShape101,
            this.lineShape100,
            this.rectangleShape11,
            this.lineShape99,
            this.lineShape98,
            this.lineShape97,
            this.lineShape96,
            this.lineShape95,
            this.lineShape94,
            this.lineShape93,
            this.lineShape92,
            this.lineShape91,
            this.lineShape44,
            this.lineShape43,
            this.lineShape42,
            this.lineShape90,
            this.lineShape89,
            this.lineShape88,
            this.lineShape87,
            this.lineShape86,
            this.lineShape85,
            this.lineShape84,
            this.lineShape83,
            this.lineShape82,
            this.lineShape81,
            this.lineShape80,
            this.lineShape79,
            this.lineShape78,
            this.lineShape77,
            this.lineShape76,
            this.lineShape75,
            this.lineShape74,
            this.lineShape73,
            this.lineShape72,
            this.lineShape71,
            this.lineShape70,
            this.lineShape69,
            this.lineShape68,
            this.lineShape67,
            this.lineShape66,
            this.lineShape65,
            this.lineShape64,
            this.lineShape63,
            this.lineShape62,
            this.lineShape61,
            this.lineShape60,
            this.lineShape59,
            this.lineShape58,
            this.lineShape57,
            this.lineShape56,
            this.lineShape55,
            this.lineShape54,
            this.lineShape53,
            this.lineShape52,
            this.lineShape51,
            this.lineShape50,
            this.lineShape49,
            this.lineShape48,
            this.lineShape47,
            this.lineShape46,
            this.lineShape45,
            this.lineShape41,
            this.lineShape40,
            this.lineShape39,
            this.lineShape38,
            this.lineShape37,
            this.lineShape36,
            this.lineShape35,
            this.lineShape34,
            this.lineShape33,
            this.lineShape32,
            this.lineShape31,
            this.lineShape30,
            this.lineShape29,
            this.lineShape28,
            this.lineShape27,
            this.lineShape26,
            this.lineShape25,
            this.lineShape24,
            this.lineShape23,
            this.lineShape22,
            this.lineShape21,
            this.lineShape20,
            this.lineShape19,
            this.lineShape18,
            this.lineShape17,
            this.lineShape16,
            this.lineShape15,
            this.lineShape14,
            this.lineShape13,
            this.lineShape12,
            this.lineShape11,
            this.lineShape10,
            this.lineShape9,
            this.lineShape8,
            this.lineShape7,
            this.lineShape6,
            this.lineShape5,
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.rectangleShape10,
            this.rectangleShape9,
            this.rectangleShape8,
            this.rectangleShape7,
            this.rectangleShape6,
            this.rectangleShape5,
            this.rectangleShape4,
            this.rectangleShape3,
            this.rectangleShape2,
            this.rectangleShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(982, 599);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape138
            // 
            this.lineShape138.BorderColor = System.Drawing.Color.Black;
            this.lineShape138.Name = "lineShape138";
            this.lineShape138.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape138.X1 = 393;
            this.lineShape138.X2 = 559;
            this.lineShape138.Y1 = 476;
            this.lineShape138.Y2 = 476;
            // 
            // lineShape137
            // 
            this.lineShape137.BorderColor = System.Drawing.Color.Black;
            this.lineShape137.BorderWidth = 2;
            this.lineShape137.Name = "lineShape137";
            this.lineShape137.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape137.X1 = 548;
            this.lineShape137.X2 = 558;
            this.lineShape137.Y1 = 471;
            this.lineShape137.Y2 = 476;
            // 
            // lineShape136
            // 
            this.lineShape136.BorderColor = System.Drawing.Color.Black;
            this.lineShape136.BorderWidth = 2;
            this.lineShape136.Name = "lineShape136";
            this.lineShape136.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape136.X1 = 548;
            this.lineShape136.X2 = 558;
            this.lineShape136.Y1 = 481;
            this.lineShape136.Y2 = 476;
            // 
            // lineShape135
            // 
            this.lineShape135.BorderColor = System.Drawing.Color.Black;
            this.lineShape135.BorderWidth = 2;
            this.lineShape135.Name = "lineShape135";
            this.lineShape135.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape135.X1 = 419;
            this.lineShape135.X2 = 429;
            this.lineShape135.Y1 = 462;
            this.lineShape135.Y2 = 467;
            // 
            // lineShape134
            // 
            this.lineShape134.BorderColor = System.Drawing.Color.Black;
            this.lineShape134.BorderWidth = 2;
            this.lineShape134.Name = "lineShape134";
            this.lineShape134.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape134.X1 = 419;
            this.lineShape134.X2 = 429;
            this.lineShape134.Y1 = 472;
            this.lineShape134.Y2 = 467;
            // 
            // lineShape133
            // 
            this.lineShape133.BorderColor = System.Drawing.Color.Black;
            this.lineShape133.Name = "lineShape133";
            this.lineShape133.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape133.X1 = 393;
            this.lineShape133.X2 = 430;
            this.lineShape133.Y1 = 467;
            this.lineShape133.Y2 = 467;
            // 
            // lineShape132
            // 
            this.lineShape132.BorderColor = System.Drawing.Color.Black;
            this.lineShape132.Name = "lineShape132";
            this.lineShape132.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape132.X1 = 391;
            this.lineShape132.X2 = 557;
            this.lineShape132.Y1 = 427;
            this.lineShape132.Y2 = 427;
            // 
            // lineShape131
            // 
            this.lineShape131.BorderColor = System.Drawing.Color.Black;
            this.lineShape131.BorderWidth = 2;
            this.lineShape131.Name = "lineShape131";
            this.lineShape131.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape131.X1 = 546;
            this.lineShape131.X2 = 556;
            this.lineShape131.Y1 = 422;
            this.lineShape131.Y2 = 427;
            // 
            // lineShape130
            // 
            this.lineShape130.BorderColor = System.Drawing.Color.Black;
            this.lineShape130.BorderWidth = 2;
            this.lineShape130.Name = "lineShape130";
            this.lineShape130.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape130.X1 = 546;
            this.lineShape130.X2 = 556;
            this.lineShape130.Y1 = 432;
            this.lineShape130.Y2 = 427;
            // 
            // lineShape129
            // 
            this.lineShape129.BorderColor = System.Drawing.Color.Black;
            this.lineShape129.BorderWidth = 2;
            this.lineShape129.Name = "lineShape129";
            this.lineShape129.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape129.X1 = 417;
            this.lineShape129.X2 = 427;
            this.lineShape129.Y1 = 413;
            this.lineShape129.Y2 = 418;
            // 
            // lineShape128
            // 
            this.lineShape128.BorderColor = System.Drawing.Color.Black;
            this.lineShape128.BorderWidth = 2;
            this.lineShape128.Name = "lineShape128";
            this.lineShape128.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape128.X1 = 417;
            this.lineShape128.X2 = 427;
            this.lineShape128.Y1 = 423;
            this.lineShape128.Y2 = 418;
            // 
            // lineShape127
            // 
            this.lineShape127.BorderColor = System.Drawing.Color.Black;
            this.lineShape127.Name = "lineShape127";
            this.lineShape127.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape127.X1 = 391;
            this.lineShape127.X2 = 428;
            this.lineShape127.Y1 = 418;
            this.lineShape127.Y2 = 418;
            // 
            // lineShape126
            // 
            this.lineShape126.BorderColor = System.Drawing.Color.Black;
            this.lineShape126.BorderWidth = 2;
            this.lineShape126.Name = "lineShape126";
            this.lineShape126.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape126.X1 = 545;
            this.lineShape126.X2 = 555;
            this.lineShape126.Y1 = 374;
            this.lineShape126.Y2 = 379;
            // 
            // lineShape125
            // 
            this.lineShape125.BorderColor = System.Drawing.Color.Black;
            this.lineShape125.BorderWidth = 2;
            this.lineShape125.Name = "lineShape125";
            this.lineShape125.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape125.X1 = 545;
            this.lineShape125.X2 = 555;
            this.lineShape125.Y1 = 384;
            this.lineShape125.Y2 = 379;
            // 
            // lineShape124
            // 
            this.lineShape124.BorderColor = System.Drawing.Color.Black;
            this.lineShape124.Name = "lineShape124";
            this.lineShape124.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape124.X1 = 390;
            this.lineShape124.X2 = 427;
            this.lineShape124.Y1 = 370;
            this.lineShape124.Y2 = 370;
            // 
            // lineShape123
            // 
            this.lineShape123.BorderColor = System.Drawing.Color.Black;
            this.lineShape123.Name = "lineShape123";
            this.lineShape123.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape123.X1 = 390;
            this.lineShape123.X2 = 556;
            this.lineShape123.Y1 = 379;
            this.lineShape123.Y2 = 379;
            // 
            // lineShape122
            // 
            this.lineShape122.BorderColor = System.Drawing.Color.Black;
            this.lineShape122.BorderWidth = 2;
            this.lineShape122.Name = "lineShape122";
            this.lineShape122.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape122.X1 = 416;
            this.lineShape122.X2 = 426;
            this.lineShape122.Y1 = 375;
            this.lineShape122.Y2 = 370;
            // 
            // lineShape121
            // 
            this.lineShape121.BorderColor = System.Drawing.Color.Black;
            this.lineShape121.BorderWidth = 2;
            this.lineShape121.Name = "lineShape121";
            this.lineShape121.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape121.X1 = 416;
            this.lineShape121.X2 = 426;
            this.lineShape121.Y1 = 365;
            this.lineShape121.Y2 = 370;
            // 
            // rectangleShape15
            // 
            this.rectangleShape15.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape15.Location = new System.Drawing.Point(375, 465);
            this.rectangleShape15.Name = "rectangleShape15";
            this.rectangleShape15.Size = new System.Drawing.Size(25, 20);
            // 
            // rectangleShape14
            // 
            this.rectangleShape14.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape14.Location = new System.Drawing.Point(374, 413);
            this.rectangleShape14.Name = "rectangleShape14";
            this.rectangleShape14.Size = new System.Drawing.Size(20, 21);
            // 
            // rectangleShape13
            // 
            this.rectangleShape13.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape13.Location = new System.Drawing.Point(375, 365);
            this.rectangleShape13.Name = "rectangleShape13";
            this.rectangleShape13.Size = new System.Drawing.Size(19, 20);
            // 
            // lineShape120
            // 
            this.lineShape120.BorderColor = System.Drawing.Color.Black;
            this.lineShape120.Name = "lineShape120";
            this.lineShape120.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape120.X1 = 450;
            this.lineShape120.X2 = 516;
            this.lineShape120.Y1 = 141;
            this.lineShape120.Y2 = 141;
            // 
            // lineShape119
            // 
            this.lineShape119.BorderColor = System.Drawing.Color.Black;
            this.lineShape119.Name = "lineShape119";
            this.lineShape119.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape119.X1 = 391;
            this.lineShape119.X2 = 430;
            this.lineShape119.Y1 = 264;
            this.lineShape119.Y2 = 264;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.Black;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape1.X1 = 69;
            this.lineShape1.X2 = 69;
            this.lineShape1.Y1 = 280;
            this.lineShape1.Y2 = 243;
            // 
            // lineShape118
            // 
            this.lineShape118.BorderColor = System.Drawing.Color.Black;
            this.lineShape118.Name = "lineShape118";
            this.lineShape118.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape118.X1 = 113;
            this.lineShape118.X2 = 132;
            this.lineShape118.Y1 = 333;
            this.lineShape118.Y2 = 333;
            // 
            // lineShape117
            // 
            this.lineShape117.BorderColor = System.Drawing.Color.Black;
            this.lineShape117.BorderWidth = 2;
            this.lineShape117.Name = "lineShape117";
            this.lineShape117.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape117.X1 = 121;
            this.lineShape117.X2 = 131;
            this.lineShape117.Y1 = 338;
            this.lineShape117.Y2 = 333;
            // 
            // lineShape116
            // 
            this.lineShape116.BorderColor = System.Drawing.Color.Black;
            this.lineShape116.BorderWidth = 2;
            this.lineShape116.Name = "lineShape116";
            this.lineShape116.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape116.X1 = 121;
            this.lineShape116.X2 = 131;
            this.lineShape116.Y1 = 328;
            this.lineShape116.Y2 = 333;
            // 
            // lineShape115
            // 
            this.lineShape115.BorderColor = System.Drawing.Color.Black;
            this.lineShape115.Name = "lineShape115";
            this.lineShape115.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape115.X1 = 113;
            this.lineShape115.X2 = 133;
            this.lineShape115.Y1 = 262;
            this.lineShape115.Y2 = 262;
            // 
            // lineShape114
            // 
            this.lineShape114.BorderColor = System.Drawing.Color.Black;
            this.lineShape114.BorderWidth = 2;
            this.lineShape114.Name = "lineShape114";
            this.lineShape114.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape114.X1 = 122;
            this.lineShape114.X2 = 132;
            this.lineShape114.Y1 = 267;
            this.lineShape114.Y2 = 262;
            // 
            // lineShape113
            // 
            this.lineShape113.BorderColor = System.Drawing.Color.Black;
            this.lineShape113.BorderWidth = 2;
            this.lineShape113.Name = "lineShape113";
            this.lineShape113.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape113.X1 = 122;
            this.lineShape113.X2 = 132;
            this.lineShape113.Y1 = 257;
            this.lineShape113.Y2 = 262;
            // 
            // lineShape112
            // 
            this.lineShape112.BorderColor = System.Drawing.Color.Black;
            this.lineShape112.Name = "lineShape112";
            this.lineShape112.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape112.X1 = 68;
            this.lineShape112.X2 = 163;
            this.lineShape112.Y1 = 176;
            this.lineShape112.Y2 = 176;
            // 
            // lineShape111
            // 
            this.lineShape111.BorderColor = System.Drawing.Color.Black;
            this.lineShape111.BorderWidth = 2;
            this.lineShape111.Name = "lineShape111";
            this.lineShape111.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape111.X1 = 69;
            this.lineShape111.X2 = 79;
            this.lineShape111.Y1 = 175;
            this.lineShape111.Y2 = 170;
            // 
            // lineShape110
            // 
            this.lineShape110.BorderColor = System.Drawing.Color.Black;
            this.lineShape110.BorderWidth = 2;
            this.lineShape110.Name = "lineShape110";
            this.lineShape110.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape110.X1 = 69;
            this.lineShape110.X2 = 79;
            this.lineShape110.Y1 = 176;
            this.lineShape110.Y2 = 181;
            // 
            // lineShape109
            // 
            this.lineShape109.BorderColor = System.Drawing.Color.Black;
            this.lineShape109.BorderWidth = 2;
            this.lineShape109.Name = "lineShape109";
            this.lineShape109.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape109.X1 = 82;
            this.lineShape109.X2 = 92;
            this.lineShape109.Y1 = 237;
            this.lineShape109.Y2 = 242;
            // 
            // lineShape108
            // 
            this.lineShape108.BorderColor = System.Drawing.Color.Black;
            this.lineShape108.BorderWidth = 2;
            this.lineShape108.Name = "lineShape108";
            this.lineShape108.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape108.X1 = 82;
            this.lineShape108.X2 = 92;
            this.lineShape108.Y1 = 247;
            this.lineShape108.Y2 = 242;
            // 
            // lineShape107
            // 
            this.lineShape107.BorderColor = System.Drawing.Color.Black;
            this.lineShape107.Name = "lineShape107";
            this.lineShape107.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape107.X1 = 69;
            this.lineShape107.X2 = 93;
            this.lineShape107.Y1 = 242;
            this.lineShape107.Y2 = 242;
            // 
            // rectangleShape12
            // 
            this.rectangleShape12.Location = new System.Drawing.Point(93, 231);
            this.rectangleShape12.Name = "rectangleShape12";
            this.rectangleShape12.Size = new System.Drawing.Size(20, 42);
            // 
            // lineShape106
            // 
            this.lineShape106.BorderColor = System.Drawing.Color.Black;
            this.lineShape106.Name = "lineShape106";
            this.lineShape106.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape106.X1 = 69;
            this.lineShape106.X2 = 69;
            this.lineShape106.Y1 = 242;
            this.lineShape106.Y2 = 178;
            // 
            // lineShape105
            // 
            this.lineShape105.BorderColor = System.Drawing.Color.Black;
            this.lineShape105.Name = "lineShape105";
            this.lineShape105.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape105.X1 = 70;
            this.lineShape105.X2 = 94;
            this.lineShape105.Y1 = 354;
            this.lineShape105.Y2 = 354;
            // 
            // lineShape104
            // 
            this.lineShape104.BorderColor = System.Drawing.Color.Black;
            this.lineShape104.BorderWidth = 2;
            this.lineShape104.Name = "lineShape104";
            this.lineShape104.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape104.X1 = 83;
            this.lineShape104.X2 = 93;
            this.lineShape104.Y1 = 359;
            this.lineShape104.Y2 = 354;
            // 
            // lineShape103
            // 
            this.lineShape103.BorderColor = System.Drawing.Color.Black;
            this.lineShape103.BorderWidth = 2;
            this.lineShape103.Name = "lineShape103";
            this.lineShape103.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape103.X1 = 83;
            this.lineShape103.X2 = 93;
            this.lineShape103.Y1 = 349;
            this.lineShape103.Y2 = 354;
            // 
            // lineShape102
            // 
            this.lineShape102.BorderColor = System.Drawing.Color.Black;
            this.lineShape102.Name = "lineShape102";
            this.lineShape102.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape102.X1 = 69;
            this.lineShape102.X2 = 69;
            this.lineShape102.Y1 = 378;
            this.lineShape102.Y2 = 354;
            // 
            // lineShape101
            // 
            this.lineShape101.BorderColor = System.Drawing.Color.Black;
            this.lineShape101.Name = "lineShape101";
            this.lineShape101.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape101.X1 = 208;
            this.lineShape101.X2 = 208;
            this.lineShape101.Y1 = 395;
            this.lineShape101.Y2 = 378;
            // 
            // lineShape100
            // 
            this.lineShape100.BorderColor = System.Drawing.Color.Black;
            this.lineShape100.Name = "lineShape100";
            this.lineShape100.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape100.X1 = 70;
            this.lineShape100.X2 = 208;
            this.lineShape100.Y1 = 378;
            this.lineShape100.Y2 = 378;
            // 
            // rectangleShape11
            // 
            this.rectangleShape11.Location = new System.Drawing.Point(93, 322);
            this.rectangleShape11.Name = "rectangleShape11";
            this.rectangleShape11.Size = new System.Drawing.Size(20, 42);
            // 
            // lineShape99
            // 
            this.lineShape99.BorderColor = System.Drawing.Color.Black;
            this.lineShape99.BorderWidth = 2;
            this.lineShape99.Name = "lineShape99";
            this.lineShape99.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape99.X1 = 38;
            this.lineShape99.X2 = 48;
            this.lineShape99.Y1 = 426;
            this.lineShape99.Y2 = 431;
            // 
            // lineShape98
            // 
            this.lineShape98.BorderColor = System.Drawing.Color.Black;
            this.lineShape98.BorderWidth = 2;
            this.lineShape98.Name = "lineShape98";
            this.lineShape98.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape98.X1 = 38;
            this.lineShape98.X2 = 48;
            this.lineShape98.Y1 = 425;
            this.lineShape98.Y2 = 420;
            // 
            // lineShape97
            // 
            this.lineShape97.BorderColor = System.Drawing.Color.Black;
            this.lineShape97.Name = "lineShape97";
            this.lineShape97.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape97.X1 = 37;
            this.lineShape97.X2 = 198;
            this.lineShape97.Y1 = 426;
            this.lineShape97.Y2 = 426;
            // 
            // lineShape96
            // 
            this.lineShape96.BorderColor = System.Drawing.Color.Black;
            this.lineShape96.BorderWidth = 2;
            this.lineShape96.Name = "lineShape96";
            this.lineShape96.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape96.X1 = 250;
            this.lineShape96.X2 = 260;
            this.lineShape96.Y1 = 398;
            this.lineShape96.Y2 = 403;
            // 
            // lineShape95
            // 
            this.lineShape95.BorderColor = System.Drawing.Color.Black;
            this.lineShape95.BorderWidth = 2;
            this.lineShape95.Name = "lineShape95";
            this.lineShape95.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape95.X1 = 250;
            this.lineShape95.X2 = 260;
            this.lineShape95.Y1 = 397;
            this.lineShape95.Y2 = 392;
            // 
            // lineShape94
            // 
            this.lineShape94.BorderColor = System.Drawing.Color.Black;
            this.lineShape94.Name = "lineShape94";
            this.lineShape94.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape94.X1 = 249;
            this.lineShape94.X2 = 446;
            this.lineShape94.Y1 = 398;
            this.lineShape94.Y2 = 398;
            // 
            // lineShape93
            // 
            this.lineShape93.BorderColor = System.Drawing.Color.Black;
            this.lineShape93.BorderWidth = 2;
            this.lineShape93.Name = "lineShape93";
            this.lineShape93.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape93.X1 = 250;
            this.lineShape93.X2 = 260;
            this.lineShape93.Y1 = 452;
            this.lineShape93.Y2 = 457;
            // 
            // lineShape92
            // 
            this.lineShape92.BorderColor = System.Drawing.Color.Black;
            this.lineShape92.BorderWidth = 2;
            this.lineShape92.Name = "lineShape92";
            this.lineShape92.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape92.X1 = 250;
            this.lineShape92.X2 = 260;
            this.lineShape92.Y1 = 451;
            this.lineShape92.Y2 = 446;
            // 
            // lineShape91
            // 
            this.lineShape91.BorderColor = System.Drawing.Color.Black;
            this.lineShape91.Name = "lineShape91";
            this.lineShape91.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape91.X1 = 249;
            this.lineShape91.X2 = 575;
            this.lineShape91.Y1 = 452;
            this.lineShape91.Y2 = 452;
            // 
            // lineShape44
            // 
            this.lineShape44.BorderColor = System.Drawing.Color.Black;
            this.lineShape44.BorderWidth = 2;
            this.lineShape44.Name = "lineShape44";
            this.lineShape44.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape44.X1 = 119;
            this.lineShape44.X2 = 129;
            this.lineShape44.Y1 = 276;
            this.lineShape44.Y2 = 281;
            // 
            // lineShape43
            // 
            this.lineShape43.BorderColor = System.Drawing.Color.Black;
            this.lineShape43.BorderWidth = 2;
            this.lineShape43.Name = "lineShape43";
            this.lineShape43.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape43.X1 = 119;
            this.lineShape43.X2 = 129;
            this.lineShape43.Y1 = 286;
            this.lineShape43.Y2 = 281;
            // 
            // lineShape42
            // 
            this.lineShape42.BorderColor = System.Drawing.Color.Black;
            this.lineShape42.Name = "lineShape42";
            this.lineShape42.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape42.X1 = 70;
            this.lineShape42.X2 = 130;
            this.lineShape42.Y1 = 281;
            this.lineShape42.Y2 = 281;
            // 
            // lineShape90
            // 
            this.lineShape90.BorderColor = System.Drawing.Color.Black;
            this.lineShape90.BorderWidth = 2;
            this.lineShape90.Name = "lineShape90";
            this.lineShape90.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape90.X1 = 81;
            this.lineShape90.X2 = 91;
            this.lineShape90.Y1 = 322;
            this.lineShape90.Y2 = 327;
            // 
            // lineShape89
            // 
            this.lineShape89.BorderColor = System.Drawing.Color.Black;
            this.lineShape89.BorderWidth = 2;
            this.lineShape89.Name = "lineShape89";
            this.lineShape89.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape89.X1 = 81;
            this.lineShape89.X2 = 91;
            this.lineShape89.Y1 = 332;
            this.lineShape89.Y2 = 327;
            // 
            // lineShape88
            // 
            this.lineShape88.BorderColor = System.Drawing.Color.Black;
            this.lineShape88.Name = "lineShape88";
            this.lineShape88.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape88.X1 = 22;
            this.lineShape88.X2 = 92;
            this.lineShape88.Y1 = 327;
            this.lineShape88.Y2 = 327;
            // 
            // lineShape87
            // 
            this.lineShape87.BorderColor = System.Drawing.Color.Black;
            this.lineShape87.Name = "lineShape87";
            this.lineShape87.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape87.X1 = 22;
            this.lineShape87.X2 = 130;
            this.lineShape87.Y1 = 312;
            this.lineShape87.Y2 = 312;
            // 
            // lineShape86
            // 
            this.lineShape86.BorderColor = System.Drawing.Color.Black;
            this.lineShape86.BorderWidth = 2;
            this.lineShape86.Name = "lineShape86";
            this.lineShape86.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape86.X1 = 119;
            this.lineShape86.X2 = 129;
            this.lineShape86.Y1 = 317;
            this.lineShape86.Y2 = 312;
            // 
            // lineShape85
            // 
            this.lineShape85.BorderColor = System.Drawing.Color.Black;
            this.lineShape85.BorderWidth = 2;
            this.lineShape85.Name = "lineShape85";
            this.lineShape85.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape85.X1 = 119;
            this.lineShape85.X2 = 129;
            this.lineShape85.Y1 = 307;
            this.lineShape85.Y2 = 312;
            // 
            // lineShape84
            // 
            this.lineShape84.BorderColor = System.Drawing.Color.Black;
            this.lineShape84.BorderWidth = 2;
            this.lineShape84.Name = "lineShape84";
            this.lineShape84.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape84.X1 = 120;
            this.lineShape84.X2 = 130;
            this.lineShape84.Y1 = 291;
            this.lineShape84.Y2 = 296;
            // 
            // lineShape83
            // 
            this.lineShape83.BorderColor = System.Drawing.Color.Black;
            this.lineShape83.BorderWidth = 2;
            this.lineShape83.Name = "lineShape83";
            this.lineShape83.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape83.X1 = 120;
            this.lineShape83.X2 = 130;
            this.lineShape83.Y1 = 301;
            this.lineShape83.Y2 = 296;
            // 
            // lineShape82
            // 
            this.lineShape82.BorderColor = System.Drawing.Color.Black;
            this.lineShape82.Name = "lineShape82";
            this.lineShape82.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape82.X1 = 22;
            this.lineShape82.X2 = 130;
            this.lineShape82.Y1 = 296;
            this.lineShape82.Y2 = 296;
            // 
            // lineShape81
            // 
            this.lineShape81.BorderColor = System.Drawing.Color.Black;
            this.lineShape81.Name = "lineShape81";
            this.lineShape81.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape81.X1 = 22;
            this.lineShape81.X2 = 92;
            this.lineShape81.Y1 = 264;
            this.lineShape81.Y2 = 264;
            // 
            // lineShape80
            // 
            this.lineShape80.BorderColor = System.Drawing.Color.Black;
            this.lineShape80.BorderWidth = 2;
            this.lineShape80.Name = "lineShape80";
            this.lineShape80.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape80.X1 = 82;
            this.lineShape80.X2 = 92;
            this.lineShape80.Y1 = 269;
            this.lineShape80.Y2 = 264;
            // 
            // lineShape79
            // 
            this.lineShape79.BorderColor = System.Drawing.Color.Black;
            this.lineShape79.BorderWidth = 2;
            this.lineShape79.Name = "lineShape79";
            this.lineShape79.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape79.X1 = 82;
            this.lineShape79.X2 = 92;
            this.lineShape79.Y1 = 259;
            this.lineShape79.Y2 = 264;
            // 
            // lineShape78
            // 
            this.lineShape78.BorderColor = System.Drawing.Color.Black;
            this.lineShape78.BorderWidth = 2;
            this.lineShape78.Name = "lineShape78";
            this.lineShape78.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape78.X1 = 321;
            this.lineShape78.X2 = 331;
            this.lineShape78.Y1 = 161;
            this.lineShape78.Y2 = 166;
            // 
            // lineShape77
            // 
            this.lineShape77.BorderColor = System.Drawing.Color.Black;
            this.lineShape77.BorderWidth = 2;
            this.lineShape77.Name = "lineShape77";
            this.lineShape77.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape77.X1 = 321;
            this.lineShape77.X2 = 331;
            this.lineShape77.Y1 = 160;
            this.lineShape77.Y2 = 155;
            // 
            // lineShape76
            // 
            this.lineShape76.BorderColor = System.Drawing.Color.Black;
            this.lineShape76.Name = "lineShape76";
            this.lineShape76.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape76.X1 = 320;
            this.lineShape76.X2 = 390;
            this.lineShape76.Y1 = 161;
            this.lineShape76.Y2 = 161;
            // 
            // lineShape75
            // 
            this.lineShape75.BorderColor = System.Drawing.Color.Black;
            this.lineShape75.Name = "lineShape75";
            this.lineShape75.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape75.X1 = 391;
            this.lineShape75.X2 = 391;
            this.lineShape75.Y1 = 264;
            this.lineShape75.Y2 = 161;
            // 
            // lineShape74
            // 
            this.lineShape74.BorderColor = System.Drawing.Color.Black;
            this.lineShape74.BorderWidth = 2;
            this.lineShape74.Name = "lineShape74";
            this.lineShape74.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape74.X1 = 420;
            this.lineShape74.X2 = 430;
            this.lineShape74.Y1 = 332;
            this.lineShape74.Y2 = 337;
            // 
            // lineShape73
            // 
            this.lineShape73.BorderColor = System.Drawing.Color.Black;
            this.lineShape73.BorderWidth = 2;
            this.lineShape73.Name = "lineShape73";
            this.lineShape73.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape73.X1 = 420;
            this.lineShape73.X2 = 430;
            this.lineShape73.Y1 = 342;
            this.lineShape73.Y2 = 337;
            // 
            // lineShape72
            // 
            this.lineShape72.BorderColor = System.Drawing.Color.Black;
            this.lineShape72.Name = "lineShape72";
            this.lineShape72.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape72.X1 = 320;
            this.lineShape72.X2 = 560;
            this.lineShape72.Y1 = 346;
            this.lineShape72.Y2 = 346;
            // 
            // lineShape71
            // 
            this.lineShape71.BorderColor = System.Drawing.Color.Black;
            this.lineShape71.BorderWidth = 2;
            this.lineShape71.Name = "lineShape71";
            this.lineShape71.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape71.X1 = 549;
            this.lineShape71.X2 = 559;
            this.lineShape71.Y1 = 341;
            this.lineShape71.Y2 = 346;
            // 
            // lineShape70
            // 
            this.lineShape70.BorderColor = System.Drawing.Color.Black;
            this.lineShape70.BorderWidth = 2;
            this.lineShape70.Name = "lineShape70";
            this.lineShape70.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape70.X1 = 549;
            this.lineShape70.X2 = 559;
            this.lineShape70.Y1 = 351;
            this.lineShape70.Y2 = 346;
            // 
            // lineShape69
            // 
            this.lineShape69.BorderColor = System.Drawing.Color.Black;
            this.lineShape69.Name = "lineShape69";
            this.lineShape69.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape69.X1 = 320;
            this.lineShape69.X2 = 431;
            this.lineShape69.Y1 = 337;
            this.lineShape69.Y2 = 337;
            // 
            // lineShape68
            // 
            this.lineShape68.BorderColor = System.Drawing.Color.Black;
            this.lineShape68.BorderWidth = 2;
            this.lineShape68.Name = "lineShape68";
            this.lineShape68.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape68.X1 = 420;
            this.lineShape68.X2 = 430;
            this.lineShape68.Y1 = 314;
            this.lineShape68.Y2 = 319;
            // 
            // lineShape67
            // 
            this.lineShape67.BorderColor = System.Drawing.Color.Black;
            this.lineShape67.BorderWidth = 2;
            this.lineShape67.Name = "lineShape67";
            this.lineShape67.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape67.X1 = 420;
            this.lineShape67.X2 = 430;
            this.lineShape67.Y1 = 324;
            this.lineShape67.Y2 = 319;
            // 
            // lineShape66
            // 
            this.lineShape66.BorderColor = System.Drawing.Color.Black;
            this.lineShape66.Name = "lineShape66";
            this.lineShape66.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape66.X1 = 320;
            this.lineShape66.X2 = 561;
            this.lineShape66.Y1 = 328;
            this.lineShape66.Y2 = 328;
            // 
            // lineShape65
            // 
            this.lineShape65.BorderColor = System.Drawing.Color.Black;
            this.lineShape65.BorderWidth = 2;
            this.lineShape65.Name = "lineShape65";
            this.lineShape65.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape65.X1 = 550;
            this.lineShape65.X2 = 560;
            this.lineShape65.Y1 = 323;
            this.lineShape65.Y2 = 328;
            // 
            // lineShape64
            // 
            this.lineShape64.BorderColor = System.Drawing.Color.Black;
            this.lineShape64.BorderWidth = 2;
            this.lineShape64.Name = "lineShape64";
            this.lineShape64.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape64.X1 = 550;
            this.lineShape64.X2 = 560;
            this.lineShape64.Y1 = 333;
            this.lineShape64.Y2 = 328;
            // 
            // lineShape63
            // 
            this.lineShape63.BorderColor = System.Drawing.Color.Black;
            this.lineShape63.Name = "lineShape63";
            this.lineShape63.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape63.X1 = 320;
            this.lineShape63.X2 = 431;
            this.lineShape63.Y1 = 319;
            this.lineShape63.Y2 = 319;
            // 
            // lineShape62
            // 
            this.lineShape62.BorderColor = System.Drawing.Color.Black;
            this.lineShape62.BorderWidth = 2;
            this.lineShape62.Name = "lineShape62";
            this.lineShape62.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape62.X1 = 419;
            this.lineShape62.X2 = 429;
            this.lineShape62.Y1 = 296;
            this.lineShape62.Y2 = 301;
            // 
            // lineShape61
            // 
            this.lineShape61.BorderColor = System.Drawing.Color.Black;
            this.lineShape61.BorderWidth = 2;
            this.lineShape61.Name = "lineShape61";
            this.lineShape61.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape61.X1 = 419;
            this.lineShape61.X2 = 429;
            this.lineShape61.Y1 = 306;
            this.lineShape61.Y2 = 301;
            // 
            // lineShape60
            // 
            this.lineShape60.BorderColor = System.Drawing.Color.Black;
            this.lineShape60.Name = "lineShape60";
            this.lineShape60.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape60.X1 = 320;
            this.lineShape60.X2 = 558;
            this.lineShape60.Y1 = 310;
            this.lineShape60.Y2 = 310;
            // 
            // lineShape59
            // 
            this.lineShape59.BorderColor = System.Drawing.Color.Black;
            this.lineShape59.BorderWidth = 2;
            this.lineShape59.Name = "lineShape59";
            this.lineShape59.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape59.X1 = 547;
            this.lineShape59.X2 = 557;
            this.lineShape59.Y1 = 305;
            this.lineShape59.Y2 = 310;
            // 
            // lineShape58
            // 
            this.lineShape58.BorderColor = System.Drawing.Color.Black;
            this.lineShape58.BorderWidth = 2;
            this.lineShape58.Name = "lineShape58";
            this.lineShape58.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape58.X1 = 547;
            this.lineShape58.X2 = 557;
            this.lineShape58.Y1 = 315;
            this.lineShape58.Y2 = 310;
            // 
            // lineShape57
            // 
            this.lineShape57.BorderColor = System.Drawing.Color.Black;
            this.lineShape57.Name = "lineShape57";
            this.lineShape57.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape57.X1 = 320;
            this.lineShape57.X2 = 430;
            this.lineShape57.Y1 = 301;
            this.lineShape57.Y2 = 301;
            // 
            // lineShape56
            // 
            this.lineShape56.BorderColor = System.Drawing.Color.Black;
            this.lineShape56.BorderWidth = 2;
            this.lineShape56.Name = "lineShape56";
            this.lineShape56.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape56.X1 = 420;
            this.lineShape56.X2 = 430;
            this.lineShape56.Y1 = 277;
            this.lineShape56.Y2 = 282;
            // 
            // lineShape55
            // 
            this.lineShape55.BorderColor = System.Drawing.Color.Black;
            this.lineShape55.BorderWidth = 2;
            this.lineShape55.Name = "lineShape55";
            this.lineShape55.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape55.X1 = 420;
            this.lineShape55.X2 = 430;
            this.lineShape55.Y1 = 287;
            this.lineShape55.Y2 = 282;
            // 
            // lineShape54
            // 
            this.lineShape54.BorderColor = System.Drawing.Color.Black;
            this.lineShape54.Name = "lineShape54";
            this.lineShape54.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape54.X1 = 320;
            this.lineShape54.X2 = 559;
            this.lineShape54.Y1 = 292;
            this.lineShape54.Y2 = 292;
            // 
            // lineShape53
            // 
            this.lineShape53.BorderColor = System.Drawing.Color.Black;
            this.lineShape53.BorderWidth = 2;
            this.lineShape53.Name = "lineShape53";
            this.lineShape53.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape53.X1 = 548;
            this.lineShape53.X2 = 558;
            this.lineShape53.Y1 = 287;
            this.lineShape53.Y2 = 292;
            // 
            // lineShape52
            // 
            this.lineShape52.BorderColor = System.Drawing.Color.Black;
            this.lineShape52.BorderWidth = 2;
            this.lineShape52.Name = "lineShape52";
            this.lineShape52.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape52.X1 = 548;
            this.lineShape52.X2 = 558;
            this.lineShape52.Y1 = 297;
            this.lineShape52.Y2 = 292;
            // 
            // lineShape51
            // 
            this.lineShape51.BorderColor = System.Drawing.Color.Black;
            this.lineShape51.Name = "lineShape51";
            this.lineShape51.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape51.X1 = 320;
            this.lineShape51.X2 = 431;
            this.lineShape51.Y1 = 282;
            this.lineShape51.Y2 = 282;
            // 
            // lineShape50
            // 
            this.lineShape50.BorderColor = System.Drawing.Color.Black;
            this.lineShape50.Name = "lineShape50";
            this.lineShape50.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape50.X1 = 320;
            this.lineShape50.X2 = 560;
            this.lineShape50.Y1 = 273;
            this.lineShape50.Y2 = 273;
            // 
            // lineShape49
            // 
            this.lineShape49.BorderColor = System.Drawing.Color.Black;
            this.lineShape49.BorderWidth = 2;
            this.lineShape49.Name = "lineShape49";
            this.lineShape49.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape49.X1 = 549;
            this.lineShape49.X2 = 559;
            this.lineShape49.Y1 = 278;
            this.lineShape49.Y2 = 273;
            // 
            // lineShape48
            // 
            this.lineShape48.BorderColor = System.Drawing.Color.Black;
            this.lineShape48.BorderWidth = 2;
            this.lineShape48.Name = "lineShape48";
            this.lineShape48.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape48.X1 = 549;
            this.lineShape48.X2 = 559;
            this.lineShape48.Y1 = 268;
            this.lineShape48.Y2 = 273;
            // 
            // lineShape47
            // 
            this.lineShape47.BorderColor = System.Drawing.Color.Black;
            this.lineShape47.Name = "lineShape47";
            this.lineShape47.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape47.X1 = 320;
            this.lineShape47.X2 = 390;
            this.lineShape47.Y1 = 264;
            this.lineShape47.Y2 = 264;
            // 
            // lineShape46
            // 
            this.lineShape46.BorderColor = System.Drawing.Color.Black;
            this.lineShape46.BorderWidth = 2;
            this.lineShape46.Name = "lineShape46";
            this.lineShape46.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape46.X1 = 420;
            this.lineShape46.X2 = 430;
            this.lineShape46.Y1 = 269;
            this.lineShape46.Y2 = 264;
            // 
            // lineShape45
            // 
            this.lineShape45.BorderColor = System.Drawing.Color.Black;
            this.lineShape45.BorderWidth = 2;
            this.lineShape45.Name = "lineShape45";
            this.lineShape45.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape45.X1 = 420;
            this.lineShape45.X2 = 430;
            this.lineShape45.Y1 = 259;
            this.lineShape45.Y2 = 264;
            // 
            // lineShape41
            // 
            this.lineShape41.BorderColor = System.Drawing.Color.Black;
            this.lineShape41.Name = "lineShape41";
            this.lineShape41.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape41.X1 = 515;
            this.lineShape41.X2 = 515;
            this.lineShape41.Y1 = 142;
            this.lineShape41.Y2 = 132;
            // 
            // lineShape40
            // 
            this.lineShape40.BorderColor = System.Drawing.Color.Black;
            this.lineShape40.Name = "lineShape40";
            this.lineShape40.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape40.X1 = 320;
            this.lineShape40.X2 = 515;
            this.lineShape40.Y1 = 132;
            this.lineShape40.Y2 = 132;
            // 
            // lineShape39
            // 
            this.lineShape39.BorderColor = System.Drawing.Color.Black;
            this.lineShape39.BorderWidth = 2;
            this.lineShape39.Name = "lineShape39";
            this.lineShape39.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape39.X1 = 321;
            this.lineShape39.X2 = 331;
            this.lineShape39.Y1 = 131;
            this.lineShape39.Y2 = 126;
            // 
            // lineShape38
            // 
            this.lineShape38.BorderColor = System.Drawing.Color.Black;
            this.lineShape38.BorderWidth = 2;
            this.lineShape38.Name = "lineShape38";
            this.lineShape38.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape38.X1 = 321;
            this.lineShape38.X2 = 331;
            this.lineShape38.Y1 = 132;
            this.lineShape38.Y2 = 137;
            // 
            // lineShape37
            // 
            this.lineShape37.BorderColor = System.Drawing.Color.Black;
            this.lineShape37.BorderWidth = 2;
            this.lineShape37.Name = "lineShape37";
            this.lineShape37.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape37.X1 = 448;
            this.lineShape37.X2 = 458;
            this.lineShape37.Y1 = 177;
            this.lineShape37.Y2 = 182;
            // 
            // lineShape36
            // 
            this.lineShape36.BorderColor = System.Drawing.Color.Black;
            this.lineShape36.BorderWidth = 2;
            this.lineShape36.Name = "lineShape36";
            this.lineShape36.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape36.X1 = 448;
            this.lineShape36.X2 = 458;
            this.lineShape36.Y1 = 176;
            this.lineShape36.Y2 = 171;
            // 
            // lineShape35
            // 
            this.lineShape35.BorderColor = System.Drawing.Color.Black;
            this.lineShape35.Name = "lineShape35";
            this.lineShape35.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape35.X1 = 447;
            this.lineShape35.X2 = 715;
            this.lineShape35.Y1 = 177;
            this.lineShape35.Y2 = 177;
            // 
            // lineShape34
            // 
            this.lineShape34.BorderColor = System.Drawing.Color.Black;
            this.lineShape34.BorderWidth = 2;
            this.lineShape34.Name = "lineShape34";
            this.lineShape34.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape34.X1 = 447;
            this.lineShape34.X2 = 457;
            this.lineShape34.Y1 = 158;
            this.lineShape34.Y2 = 163;
            // 
            // lineShape33
            // 
            this.lineShape33.BorderColor = System.Drawing.Color.Black;
            this.lineShape33.BorderWidth = 2;
            this.lineShape33.Name = "lineShape33";
            this.lineShape33.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape33.X1 = 447;
            this.lineShape33.X2 = 457;
            this.lineShape33.Y1 = 157;
            this.lineShape33.Y2 = 152;
            // 
            // lineShape32
            // 
            this.lineShape32.BorderColor = System.Drawing.Color.Black;
            this.lineShape32.Name = "lineShape32";
            this.lineShape32.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape32.X1 = 446;
            this.lineShape32.X2 = 710;
            this.lineShape32.Y1 = 158;
            this.lineShape32.Y2 = 158;
            // 
            // lineShape31
            // 
            this.lineShape31.BorderColor = System.Drawing.Color.Black;
            this.lineShape31.BorderWidth = 2;
            this.lineShape31.Name = "lineShape31";
            this.lineShape31.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape31.X1 = 448;
            this.lineShape31.X2 = 458;
            this.lineShape31.Y1 = 141;
            this.lineShape31.Y2 = 146;
            // 
            // lineShape30
            // 
            this.lineShape30.BorderColor = System.Drawing.Color.Black;
            this.lineShape30.BorderWidth = 2;
            this.lineShape30.Name = "lineShape30";
            this.lineShape30.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape30.X1 = 448;
            this.lineShape30.X2 = 458;
            this.lineShape30.Y1 = 140;
            this.lineShape30.Y2 = 135;
            // 
            // lineShape29
            // 
            this.lineShape29.BorderColor = System.Drawing.Color.Black;
            this.lineShape29.Name = "lineShape29";
            this.lineShape29.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape29.X1 = 515;
            this.lineShape29.X2 = 715;
            this.lineShape29.Y1 = 141;
            this.lineShape29.Y2 = 141;
            // 
            // lineShape28
            // 
            this.lineShape28.BorderColor = System.Drawing.Color.Black;
            this.lineShape28.BorderWidth = 2;
            this.lineShape28.Name = "lineShape28";
            this.lineShape28.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape28.X1 = 576;
            this.lineShape28.X2 = 586;
            this.lineShape28.Y1 = 186;
            this.lineShape28.Y2 = 191;
            // 
            // lineShape27
            // 
            this.lineShape27.BorderColor = System.Drawing.Color.Black;
            this.lineShape27.BorderWidth = 2;
            this.lineShape27.Name = "lineShape27";
            this.lineShape27.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape27.X1 = 576;
            this.lineShape27.X2 = 586;
            this.lineShape27.Y1 = 185;
            this.lineShape27.Y2 = 180;
            // 
            // lineShape26
            // 
            this.lineShape26.BorderColor = System.Drawing.Color.Black;
            this.lineShape26.Name = "lineShape26";
            this.lineShape26.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape26.X1 = 575;
            this.lineShape26.X2 = 715;
            this.lineShape26.Y1 = 186;
            this.lineShape26.Y2 = 186;
            // 
            // lineShape25
            // 
            this.lineShape25.BorderColor = System.Drawing.Color.Black;
            this.lineShape25.BorderWidth = 2;
            this.lineShape25.Name = "lineShape25";
            this.lineShape25.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape25.X1 = 575;
            this.lineShape25.X2 = 585;
            this.lineShape25.Y1 = 167;
            this.lineShape25.Y2 = 172;
            // 
            // lineShape24
            // 
            this.lineShape24.BorderColor = System.Drawing.Color.Black;
            this.lineShape24.BorderWidth = 2;
            this.lineShape24.Name = "lineShape24";
            this.lineShape24.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape24.X1 = 575;
            this.lineShape24.X2 = 585;
            this.lineShape24.Y1 = 166;
            this.lineShape24.Y2 = 161;
            // 
            // lineShape23
            // 
            this.lineShape23.BorderColor = System.Drawing.Color.Black;
            this.lineShape23.Name = "lineShape23";
            this.lineShape23.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape23.X1 = 574;
            this.lineShape23.X2 = 715;
            this.lineShape23.Y1 = 167;
            this.lineShape23.Y2 = 167;
            // 
            // lineShape22
            // 
            this.lineShape22.BorderColor = System.Drawing.Color.Black;
            this.lineShape22.BorderWidth = 2;
            this.lineShape22.Name = "lineShape22";
            this.lineShape22.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape22.X1 = 576;
            this.lineShape22.X2 = 586;
            this.lineShape22.Y1 = 150;
            this.lineShape22.Y2 = 155;
            // 
            // lineShape21
            // 
            this.lineShape21.BorderColor = System.Drawing.Color.Black;
            this.lineShape21.BorderWidth = 2;
            this.lineShape21.Name = "lineShape21";
            this.lineShape21.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape21.X1 = 576;
            this.lineShape21.X2 = 586;
            this.lineShape21.Y1 = 149;
            this.lineShape21.Y2 = 144;
            // 
            // lineShape20
            // 
            this.lineShape20.BorderColor = System.Drawing.Color.Black;
            this.lineShape20.Name = "lineShape20";
            this.lineShape20.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape20.X1 = 575;
            this.lineShape20.X2 = 715;
            this.lineShape20.Y1 = 150;
            this.lineShape20.Y2 = 150;
            // 
            // lineShape19
            // 
            this.lineShape19.BorderColor = System.Drawing.Color.Black;
            this.lineShape19.Name = "lineShape19";
            this.lineShape19.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape19.X1 = 867;
            this.lineShape19.X2 = 960;
            this.lineShape19.Y1 = 148;
            this.lineShape19.Y2 = 148;
            // 
            // lineShape18
            // 
            this.lineShape18.BorderColor = System.Drawing.Color.Black;
            this.lineShape18.BorderWidth = 2;
            this.lineShape18.Name = "lineShape18";
            this.lineShape18.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape18.X1 = 868;
            this.lineShape18.X2 = 878;
            this.lineShape18.Y1 = 147;
            this.lineShape18.Y2 = 142;
            // 
            // lineShape17
            // 
            this.lineShape17.BorderColor = System.Drawing.Color.Black;
            this.lineShape17.BorderWidth = 2;
            this.lineShape17.Name = "lineShape17";
            this.lineShape17.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape17.X1 = 868;
            this.lineShape17.X2 = 878;
            this.lineShape17.Y1 = 148;
            this.lineShape17.Y2 = 153;
            // 
            // lineShape16
            // 
            this.lineShape16.BorderColor = System.Drawing.Color.Black;
            this.lineShape16.Name = "lineShape16";
            this.lineShape16.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape16.X1 = 866;
            this.lineShape16.X2 = 960;
            this.lineShape16.Y1 = 165;
            this.lineShape16.Y2 = 165;
            // 
            // lineShape15
            // 
            this.lineShape15.BorderColor = System.Drawing.Color.Black;
            this.lineShape15.BorderWidth = 2;
            this.lineShape15.Name = "lineShape15";
            this.lineShape15.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape15.X1 = 867;
            this.lineShape15.X2 = 877;
            this.lineShape15.Y1 = 164;
            this.lineShape15.Y2 = 159;
            // 
            // lineShape14
            // 
            this.lineShape14.BorderColor = System.Drawing.Color.Black;
            this.lineShape14.BorderWidth = 2;
            this.lineShape14.Name = "lineShape14";
            this.lineShape14.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape14.X1 = 867;
            this.lineShape14.X2 = 877;
            this.lineShape14.Y1 = 165;
            this.lineShape14.Y2 = 170;
            // 
            // lineShape13
            // 
            this.lineShape13.BorderColor = System.Drawing.Color.Black;
            this.lineShape13.Name = "lineShape13";
            this.lineShape13.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape13.X1 = 867;
            this.lineShape13.X2 = 960;
            this.lineShape13.Y1 = 184;
            this.lineShape13.Y2 = 184;
            // 
            // lineShape12
            // 
            this.lineShape12.BorderColor = System.Drawing.Color.Black;
            this.lineShape12.BorderWidth = 2;
            this.lineShape12.Name = "lineShape12";
            this.lineShape12.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape12.X1 = 868;
            this.lineShape12.X2 = 878;
            this.lineShape12.Y1 = 183;
            this.lineShape12.Y2 = 178;
            // 
            // lineShape11
            // 
            this.lineShape11.BorderColor = System.Drawing.Color.Black;
            this.lineShape11.BorderWidth = 2;
            this.lineShape11.Name = "lineShape11";
            this.lineShape11.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape11.X1 = 868;
            this.lineShape11.X2 = 878;
            this.lineShape11.Y1 = 184;
            this.lineShape11.Y2 = 189;
            // 
            // lineShape10
            // 
            this.lineShape10.BorderColor = System.Drawing.Color.Black;
            this.lineShape10.Name = "lineShape10";
            this.lineShape10.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape10.X1 = 445;
            this.lineShape10.X2 = 715;
            this.lineShape10.Y1 = 485;
            this.lineShape10.Y2 = 485;
            // 
            // lineShape9
            // 
            this.lineShape9.BorderColor = System.Drawing.Color.Black;
            this.lineShape9.BorderWidth = 2;
            this.lineShape9.Name = "lineShape9";
            this.lineShape9.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape9.X1 = 446;
            this.lineShape9.X2 = 456;
            this.lineShape9.Y1 = 484;
            this.lineShape9.Y2 = 479;
            // 
            // lineShape8
            // 
            this.lineShape8.BorderColor = System.Drawing.Color.Black;
            this.lineShape8.BorderWidth = 2;
            this.lineShape8.Name = "lineShape8";
            this.lineShape8.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape8.X1 = 446;
            this.lineShape8.X2 = 456;
            this.lineShape8.Y1 = 485;
            this.lineShape8.Y2 = 490;
            // 
            // lineShape7
            // 
            this.lineShape7.BorderColor = System.Drawing.Color.Black;
            this.lineShape7.Name = "lineShape7";
            this.lineShape7.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape7.X1 = 573;
            this.lineShape7.X2 = 715;
            this.lineShape7.Y1 = 508;
            this.lineShape7.Y2 = 508;
            // 
            // lineShape6
            // 
            this.lineShape6.BorderColor = System.Drawing.Color.Black;
            this.lineShape6.BorderWidth = 2;
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape6.X1 = 574;
            this.lineShape6.X2 = 584;
            this.lineShape6.Y1 = 507;
            this.lineShape6.Y2 = 502;
            // 
            // lineShape5
            // 
            this.lineShape5.BorderColor = System.Drawing.Color.Black;
            this.lineShape5.BorderWidth = 2;
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape5.X1 = 574;
            this.lineShape5.X2 = 584;
            this.lineShape5.Y1 = 508;
            this.lineShape5.Y2 = 513;
            // 
            // lineShape4
            // 
            this.lineShape4.BorderColor = System.Drawing.Color.Black;
            this.lineShape4.BorderWidth = 2;
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape4.X1 = 869;
            this.lineShape4.X2 = 879;
            this.lineShape4.Y1 = 386;
            this.lineShape4.Y2 = 391;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderColor = System.Drawing.Color.Black;
            this.lineShape3.BorderWidth = 2;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape3.X1 = 869;
            this.lineShape3.X2 = 879;
            this.lineShape3.Y1 = 385;
            this.lineShape3.Y2 = 380;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.Black;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.SelectionColor = System.Drawing.Color.Transparent;
            this.lineShape2.X1 = 868;
            this.lineShape2.X2 = 960;
            this.lineShape2.Y1 = 386;
            this.lineShape2.Y2 = 386;
            // 
            // rectangleShape10
            // 
            this.rectangleShape10.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rectangleShape10.FillGradientColor = System.Drawing.Color.DimGray;
            this.rectangleShape10.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape10.Location = new System.Drawing.Point(689, 120);
            this.rectangleShape10.Name = "rectangleShape10";
            this.rectangleShape10.Size = new System.Drawing.Size(178, 78);
            // 
            // rectangleShape9
            // 
            this.rectangleShape9.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rectangleShape9.FillGradientColor = System.Drawing.Color.DimGray;
            this.rectangleShape9.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape9.Location = new System.Drawing.Point(136, 56);
            this.rectangleShape9.Name = "rectangleShape9";
            this.rectangleShape9.Size = new System.Drawing.Size(183, 156);
            // 
            // rectangleShape8
            // 
            this.rectangleShape8.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rectangleShape8.FillGradientColor = System.Drawing.Color.DimGray;
            this.rectangleShape8.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape8.Location = new System.Drawing.Point(136, 235);
            this.rectangleShape8.Name = "rectangleShape8";
            this.rectangleShape8.Size = new System.Drawing.Size(183, 116);
            // 
            // rectangleShape7
            // 
            this.rectangleShape7.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rectangleShape7.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape7.Location = new System.Drawing.Point(690, 225);
            this.rectangleShape7.Name = "rectangleShape7";
            this.rectangleShape7.Size = new System.Drawing.Size(178, 304);
            // 
            // rectangleShape6
            // 
            this.rectangleShape6.Location = new System.Drawing.Point(21, 13);
            this.rectangleShape6.Name = "rectangleShape6";
            this.rectangleShape6.Size = new System.Drawing.Size(939, 23);
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.BorderWidth = 2;
            this.rectangleShape5.Location = new System.Drawing.Point(560, 92);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.Size = new System.Drawing.Size(15, 450);
            // 
            // rectangleShape4
            // 
            this.rectangleShape4.BorderWidth = 2;
            this.rectangleShape4.Location = new System.Drawing.Point(431, 92);
            this.rectangleShape4.Name = "rectangleShape4";
            this.rectangleShape4.Size = new System.Drawing.Size(15, 450);
            // 
            // rectangleShape3
            // 
            this.rectangleShape3.BorderWidth = 2;
            this.rectangleShape3.Location = new System.Drawing.Point(23, 51);
            this.rectangleShape3.Name = "rectangleShape3";
            this.rectangleShape3.Size = new System.Drawing.Size(15, 517);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BorderWidth = 2;
            this.rectangleShape2.Location = new System.Drawing.Point(23, 568);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(937, 15);
            // 
            // rectangleShape1
            // 
            this.rectangleShape1.BorderWidth = 2;
            this.rectangleShape1.Location = new System.Drawing.Point(945, 51);
            this.rectangleShape1.Name = "rectangleShape1";
            this.rectangleShape1.Size = new System.Drawing.Size(15, 517);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listView7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(988, 605);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Programul";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listView7
            // 
            this.listView7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView7.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listView7.Location = new System.Drawing.Point(3, 3);
            this.listView7.Name = "listView7";
            this.listView7.Size = new System.Drawing.Size(982, 599);
            this.listView7.TabIndex = 0;
            this.listView7.UseCompatibleStateImageBehavior = false;
            this.listView7.View = System.Windows.Forms.View.Details;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listView6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(988, 605);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Microcodul";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listView6
            // 
            this.listView6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView6.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listView6.Location = new System.Drawing.Point(3, 3);
            this.listView6.Name = "listView6";
            this.listView6.Size = new System.Drawing.Size(982, 599);
            this.listView6.TabIndex = 0;
            this.listView6.UseCompatibleStateImageBehavior = false;
            this.listView6.View = System.Windows.Forms.View.Details;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.stepMC,
            this.stepMI,
            this.stepI,
            this.run,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripSeparator1,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(4, 24);
            this.toolStrip1.MinimumSize = new System.Drawing.Size(1000, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1000, 25);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::View.Properties.Resources.jload_obj;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(129, 22);
            this.toolStripButton1.Text = "Incarca Microcodul";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::View.Properties.Resources.fldr_obj;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(124, 22);
            this.toolStripButton2.Text = "Incarca Programul";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // stepMC
            // 
            this.stepMC.Image = global::View.Properties.Resources.start_task;
            this.stepMC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepMC.Name = "stepMC";
            this.stepMC.Size = new System.Drawing.Size(151, 22);
            this.stepMC.Text = "Executa Microcomanda";
            this.stepMC.Click += new System.EventHandler(this.stepMC_Click);
            // 
            // stepMI
            // 
            this.stepMI.Image = global::View.Properties.Resources.runlast_co;
            this.stepMI.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepMI.Name = "stepMI";
            this.stepMI.Size = new System.Drawing.Size(164, 22);
            this.stepMI.Text = "Executa Microinstructiune";
            this.stepMI.Click += new System.EventHandler(this.stepMI_Click);
            // 
            // stepI
            // 
            this.stepI.Image = global::View.Properties.Resources.restart_task;
            this.stepI.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepI.Name = "stepI";
            this.stepI.Size = new System.Drawing.Size(133, 22);
            this.stepI.Text = "Executa Instructiune";
            this.stepI.Click += new System.EventHandler(this.stepI_Click);
            // 
            // run
            // 
            this.run.Image = global::View.Properties.Resources.lrun_obj;
            this.run.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.run.Name = "run";
            this.run.Size = new System.Drawing.Size(48, 22);
            this.run.Text = "Run";
            this.run.Click += new System.EventHandler(this.run_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::View.Properties.Resources.suspend_co;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(58, 22);
            this.toolStripButton4.Text = "Pauza";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::View.Properties.Resources.terminatedlaunch_obj;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(55, 22);
            this.toolStripButton3.Text = "Reset";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fisierToolStripMenuItem,
            this.optiuniToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(996, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fisierToolStripMenuItem
            // 
            this.fisierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.incarcaMicrocodulToolStripMenuItem,
            this.incarcaProgramulToolStripMenuItem,
            this.salveazaFisierulBinarToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fisierToolStripMenuItem.Name = "fisierToolStripMenuItem";
            this.fisierToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.fisierToolStripMenuItem.Text = "Fisier";
            // 
            // incarcaMicrocodulToolStripMenuItem
            // 
            this.incarcaMicrocodulToolStripMenuItem.Name = "incarcaMicrocodulToolStripMenuItem";
            this.incarcaMicrocodulToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.incarcaMicrocodulToolStripMenuItem.Text = "Incarca Microcodul";
            this.incarcaMicrocodulToolStripMenuItem.Click += new System.EventHandler(this.incarcaMicrocodulToolStripMenuItem_Click);
            // 
            // incarcaProgramulToolStripMenuItem
            // 
            this.incarcaProgramulToolStripMenuItem.Name = "incarcaProgramulToolStripMenuItem";
            this.incarcaProgramulToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.incarcaProgramulToolStripMenuItem.Text = "Incarca Programul";
            this.incarcaProgramulToolStripMenuItem.Click += new System.EventHandler(this.incarcaProgramulToolStripMenuItem_Click);
            // 
            // salveazaFisierulBinarToolStripMenuItem
            // 
            this.salveazaFisierulBinarToolStripMenuItem.Name = "salveazaFisierulBinarToolStripMenuItem";
            this.salveazaFisierulBinarToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.salveazaFisierulBinarToolStripMenuItem.Text = "Salveaza Fisierul Binar";
            this.salveazaFisierulBinarToolStripMenuItem.Click += new System.EventHandler(this.salveazaFisierulBinarToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // optiuniToolStripMenuItem
            // 
            this.optiuniToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afisareBaza10ToolStripMenuItem,
            this.afisareBaza2ToolStripMenuItem,
            this.afisareBaza16ToolStripMenuItem});
            this.optiuniToolStripMenuItem.Name = "optiuniToolStripMenuItem";
            this.optiuniToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.optiuniToolStripMenuItem.Text = "Optiuni";
            // 
            // afisareBaza10ToolStripMenuItem
            // 
            this.afisareBaza10ToolStripMenuItem.Name = "afisareBaza10ToolStripMenuItem";
            this.afisareBaza10ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.afisareBaza10ToolStripMenuItem.Text = "Afisare baza 10";
            this.afisareBaza10ToolStripMenuItem.Click += new System.EventHandler(this.afisareBaza10ToolStripMenuItem_Click);
            // 
            // afisareBaza2ToolStripMenuItem
            // 
            this.afisareBaza2ToolStripMenuItem.Name = "afisareBaza2ToolStripMenuItem";
            this.afisareBaza2ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.afisareBaza2ToolStripMenuItem.Text = "Afisare baza 2";
            this.afisareBaza2ToolStripMenuItem.Click += new System.EventHandler(this.afisareBaza2ToolStripMenuItem_Click);
            // 
            // afisareBaza16ToolStripMenuItem
            // 
            this.afisareBaza16ToolStripMenuItem.Name = "afisareBaza16ToolStripMenuItem";
            this.afisareBaza16ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.afisareBaza16ToolStripMenuItem.Text = "Afisare baza 16";
            this.afisareBaza16ToolStripMenuItem.Click += new System.EventHandler(this.afisareBaza16ToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::View.Properties.Resources.runlast_co;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(88, 22);
            this.toolStripButton5.Text = "Executa Tot";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 679);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Simulator CPU Microprogramat";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.ListView listView4;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ListView listView1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape44;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape43;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape42;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape90;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape89;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape88;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape87;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape86;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape85;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape84;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape83;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape82;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape81;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape80;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape79;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape78;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape77;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape76;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape75;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape74;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape73;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape72;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape71;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape70;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape69;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape68;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape67;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape66;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape65;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape64;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape63;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape62;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape61;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape60;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape59;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape58;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape57;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape56;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape55;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape54;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape53;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape52;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape51;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape50;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape49;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape48;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape47;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape46;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape45;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape41;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape40;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape39;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape38;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape37;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape36;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape35;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape34;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape33;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape32;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape31;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape30;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape29;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape28;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape27;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape26;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape25;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape24;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape23;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape22;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape21;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape20;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape19;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape18;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape17;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape16;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape15;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape14;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape13;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape12;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape11;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape10;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape9;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape10;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape9;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape8;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape7;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape6;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape4;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape3;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape93;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape92;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape91;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape96;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape95;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape94;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape99;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape98;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape97;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape105;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape104;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape103;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape102;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape101;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape100;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape11;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape12;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape106;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape112;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape111;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape110;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape109;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape108;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape107;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape118;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape117;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape116;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape115;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape114;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape113;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape120;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape119;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape138;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape137;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape136;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape135;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape134;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape133;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape132;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape131;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape130;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape129;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape128;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape127;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape126;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape125;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape124;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape123;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape122;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape121;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape15;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape14;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape13;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton stepMC;
        private System.Windows.Forms.ToolStripButton stepMI;
        private System.Windows.Forms.ToolStripButton stepI;
        private System.Windows.Forms.ToolStripButton run;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fisierToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem incarcaMicrocodulToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incarcaProgramulToolStripMenuItem;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListView listView5;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optiuniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afisareBaza10ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afisareBaza2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afisareBaza16ToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ListView listView6;
        private System.Windows.Forms.ListView listView7;
        private System.Windows.Forms.ToolStripMenuItem salveazaFisierulBinarToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
    }
}

