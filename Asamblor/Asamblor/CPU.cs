﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class CPUMicroInstructiune
    {
        static readonly int MASCA_1B = 0x00ff,
            MASCA_5b = 0x001f,
            MASCA_4b = 0x000f,
            MASCA_3b = 0x0007,
            MASCA_2b = 0x0003,
            MASCA_1b = 0x0001;
        public CPUMicroInstructiune(Int64 MIR)
        {
            tf = ((0x100 & MIR) == 0) ? false : true;
            if (tf)
                c = (CondRamif)(MASCA_4b & (MIR >> 12));
            else
                c = (CondRamifFalse)(MASCA_4b & (MIR >> 12));
            i = (Index)(MASCA_3b & (MIR >> 9));
            adr = (short)(MASCA_1B & MIR);
            o = (OpOTHER)(MASCA_5b & (MIR >> 16));
            m = (OpMEM)(MASCA_2b & (MIR >> 21));
            r = (OpRBUS)(MASCA_4b & (MIR >> 23));
            a = (OpALU)(MASCA_4b & (MIR >> 27));
            d = (OpDBUS)(MASCA_4b & (MIR >> 31));
            s = (OpSBUS)(MASCA_4b & (MIR >> 35));
        }
        public OpSBUS s;
        public OpDBUS d;
        public OpRBUS r;
        public OpALU a;
        public OpMEM m;
        public OpOTHER o;
        internal System.Enum c;

        public System.Enum C
        {
            get
            {
                if (c.GetType() == typeof(CondRamif)) return (CondRamif)c;
                else return (CondRamifFalse)c;
            }
            set { c = value; }
        }
        public bool tf;
        public Index i;
        public short adr;
    }
    public class CPUInstructiune
    {
        public InstructionClass c;
        public CPUInstructiune(UInt16 IR)
        {
            opcode = 0;
            mad = 0;
            mas = 0;
            offset = 0;
            rd = 0;
            rs = 0;
            int IR15 = IR >> 15, IR14 = (0x01 & (IR >> 14)), IR13 = (0x01 & (IR >> 13));
            int CL0 = IR15 & IR14, CL1 = IR15 & (~IR14 | IR13);
            int cl = CL1 | (CL0 << 1);
            c = (InstructionClass)cl;

            switch (this.c)
            {
                case InstructionClass.B1:
                    {
                        opcode = (ushort)(IR >> 12);
                        try
                        {
                            Asamblor.GetInstructionClass(opcode);
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        mas = (AddressingMode)(0x0003 & (IR >> 10));
                        mad = (AddressingMode)(0x0003 & (IR >> 4));
                        rs = (Registers)(0x000f & (IR >> 6));
                        rd = (Registers)(0x000f & (IR));
                    }
                    break;
                case InstructionClass.B2:
                    {
                        opcode = (ushort)(IR >> 6);
                        try
                        {
                            Asamblor.GetInstructionClass(opcode);
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        opcode = (ushort)(0x007f & (IR >> 6));
                        mad = (AddressingMode)(0x0003 & (IR >> 4));
                        rd = (Registers)(0x000f & (IR));
                    }
                    break;
                case InstructionClass.B3:
                    {
                        opcode = (ushort)(IR >> 8);
                        try
                        {
                            Asamblor.GetInstructionClass(opcode);
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        opcode = (ushort)(0x1f & opcode);
                        offset = (short)(0x00ff & IR);
                    }
                    break;
                case InstructionClass.B4:
                    {
                        opcode = (ushort)(IR);
                        try
                        {
                            Asamblor.GetInstructionClass(opcode);
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        opcode = (ushort)(0x1fff & IR);
                    }
                    break;
                default:
                    break;
            }

        }
        public ushort opcode;
        public AddressingMode mas;
        public AddressingMode mad;
        public Registers rs;
        public Registers rd;
        public short offset;
    }
    public class CPU
    {
        static readonly int MASCA_1B = 0x00ff,
            MASCA_5b = 0x001f,
            MASCA_4b = 0x000f,
            MASCA_3b = 0x0007,
            MASCA_2b = 0x0003,
            MASCA_1b = 0x0001,
            MASCA_C = 0x0001,
            MASCA_S = 0x0002,
            MASCA_Z = 0x0004,
            MASCA_V = 0x0008,
            MASCA_BVI = 0X0010,
            MASCA_ACLOW = 0X0020;
        static int memSize = 32768, mpmSize = 256, startPC = 32;


        UInt16[] Memory, InitialMemory;
        UInt16 MDR, ADR, IVR, PC, T, SP = (ushort)(memSize - 1), FLAG, IR, tempFLAG;

        Int64[] MPM;
        Int64 MIR, MAR;
        bool cil, aclow, intr;

        bool functiaF;
        int index;

        public UInt16[] Mem
        {
            get { return Memory; }
            set { Memory = value; }
        }
        

        public UInt16 Ir
        {
            get { return IR; }
            set { IR = value; }
        }

        public UInt16 Flag
        {
            get { return FLAG; }
            set { FLAG = value; }
        }

        public UInt16 Sp
        {
            get { return SP; }
            set { SP = value; }
        }

        public UInt16 T1
        {
            get { return T; }
            set { T = value; }
        }

        public UInt16 Pc
        {
            get { return PC; }
            set { PC = value; }
        }

        public UInt16 Ivr
        {
            get { return IVR; }
            set { IVR = value; }
        }

        public UInt16 Adr
        {
            get { return ADR; }
            set { ADR = value; }
        }

        public UInt16 Mdr
        {
            get { return MDR; }
            set { MDR = value; }
        }
        UInt16[] RG;

        public UInt16[] GeneralRegisters
        {
            get { return RG; }
        }
        UInt16 sbus, dbus, rbus;

        public UInt16 Rbus
        {
            get { return rbus; }
            set { rbus = value; }
        }

        

        public Int64 Mar
        {
            get { return MAR; }
        }

        

        CPUMicroInstructiune mi;

        public CPUMicroInstructiune MicroInstructiune
        {
            get { return mi; }
            set { mi = value; }
        }
        CPUInstructiune instructiune;

        public CPUInstructiune Instructiune
        {
            get { return instructiune; }
            set { instructiune = value; }
        }
        public CPU(UInt16[] code, Int64[] microCode, int startPC, int memSize)
        {
            CPU.memSize = memSize;
            CPU.startPC = startPC;
            SP = (ushort)(memSize - 1);
            Memory = new UInt16[memSize];
            InitialMemory = new UInt16[memSize];
            code.CopyTo(InitialMemory, startPC);
            RG = new UInt16[16];
            MPM = new long[mpmSize];
            code.CopyTo(Memory, startPC);
            this.MPM = microCode;
            PC = (UInt16)startPC;
            MIR = 0;
            MAR = 0;
            MIR = MPM[MAR];
        }
        public void Reset()
        {
            PC = (ushort)startPC;
            MAR = 0;
            FLAG = 0;
            aclow = false;
            intr = false;
            for (int i = 0; i < RG.Length; i++)
            {
                RG[i] = 0;
            }
            Memory.Initialize();
            InitialMemory.CopyTo(Memory, 0);
        }
        public void StepMicroInstructiune()
        {

            MIR = MPM[MAR];
            if (aclow)
                throw new ApplicationException("Program execution ended with HALT");
            mi = new CPUMicroInstructiune(MIR);
            SBUS(mi.s);
            DBUS(mi.d);
            ALU(mi.a);
            RBUS(mi.r);

            MEM(mi.m);
            OTHEROP(mi.o);

            CalculateIndex(mi.i);
            CheckRamif(mi.c);
            NextMAR();

        }
        public void StepInstructiune()
        {
            if (mi == null)
                mi = new CPUMicroInstructiune(MPM[MAR]);
            else
                do
                {
                    StepMicroInstructiune();
                    if (aclow)
                        throw new ApplicationException("Program execution ended with HALT");


                } while (mi.m != OpMEM.IFCH);
        }
        private static int microStepCount = 0;
        public void Run()
        {
            while (true)
                StepInstructiune();
        }
        public int StepMicroComanda()
        {
            int ret = 0;
            if (aclow)
                throw new ApplicationException("Program execution ended with HALT");
            switch (microStepCount)
            {
                case 0://fetch microinstructiune si sbus
                    {
                        MIR = MPM[MAR];
                        mi = new CPUMicroInstructiune(MIR);
                        SBUS(mi.s);
                        ret = microStepCount++;
                    }
                    break;
                case 1://dbus
                    {
                        DBUS(mi.d);
                        ret = microStepCount++;
                    }
                    break;
                case 2://alu
                    {
                        ALU(mi.a);
                        ret = microStepCount++;
                    }
                    break;
                case 3://rbus
                    {
                        RBUS(mi.r);
                        ret = microStepCount++;
                    }
                    break;
                case 4://mem
                    {
                        MEM(mi.m);
                        ret = microStepCount++;
                    }
                    break;
                case 5://other
                    {
                        OTHEROP(mi.o);

                        ret = microStepCount;
                        microStepCount++;
                    }
                    break;
                case 6://jump
                    {
                        CalculateIndex(mi.i);
                        CheckRamif(mi.c);
                        NextMAR();
                        ret = microStepCount;
                        microStepCount = 0;
                    }
                    break;
                default: microStepCount = 0;
                    break;
            }
            return ret;
        }

        internal void NextMAR()
        {
            if (functiaF)
                MAR = mi.adr + index;
            else
                MAR++;
        }
        internal void CheckRamif(Enum arg)
        {
            if (mi.tf)
            {
                var v = (CondRamif)arg;
                switch (v)
                {
                    case CondRamif.STEP: functiaF = false;
                        break;
                    case CondRamif.JUMPI: functiaF = true;
                        break;
                    case CondRamif.IF_B1_JUMPI_ELSE_STEP:
                        if (instructiune.c == InstructionClass.B1)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_B2_JUMPI_ELSE_STEP:
                        if (instructiune.c == InstructionClass.B2)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_B3_JUMPI_ELSE_STEP:
                        if (instructiune.c == InstructionClass.B3)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_B4_JUMPI_ELSE_STEP:
                        if (instructiune.c == InstructionClass.B4)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_RD_JUMPI_ELSE_STEP:
                        if (instructiune.mad == AddressingMode.AI)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_C_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_C) > 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_S_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_S) > 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_Z_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_Z) > 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_V_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_Z) > 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_CIL_JUMPI_ELSE_STEP:
                        if (cil)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamif.IF_ACLOW_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_ACLOW) != 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                var v = (CondRamifFalse)arg;
                switch (v)
                {
                    case CondRamifFalse.STEP: functiaF = false;
                        break;
                    case CondRamifFalse.JUMPI: functiaF = true;
                        break;
                    case CondRamifFalse.IF_NB1_JUMPI_ELSE_STEP:
                        if (instructiune.c != InstructionClass.B1)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NB2_JUMPI_ELSE_STEP:
                        if (instructiune.c != InstructionClass.B2)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NB3_JUMPI_ELSE_STEP:
                        if (instructiune.c != InstructionClass.B3)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NB4_JUMPI_ELSE_STEP:
                        if (instructiune.c != InstructionClass.B4)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NRD_JUMPI_ELSE_STEP:
                        if (instructiune.mad != AddressingMode.AD)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NC_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_C) == 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NS_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_S) == 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NZ_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_Z) == 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_NV_JUMPI_ELSE_STEP:
                        if ((FLAG & MASCA_Z) == 0)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    case CondRamifFalse.IF_INTR_JUMPI_ELSE_STEP:
                        if (intr)
                            functiaF = true;
                        else
                            functiaF = false;
                        break;
                    default:
                        break;
                }
            }
        }
        internal void CalculateIndex(Index i)
        {
            switch (i)
            {
                case Index.INDEX0:
                    index = 0;
                    break;
                case Index.INDEX1: index = (int)instructiune.c;//selectie clasa instructiunii
                    break;
                case Index.INDEX2: index = ((int)instructiune.mas) << 1;//selectie mas
                    break;
                case Index.INDEX3: index = ((int)instructiune.mad) << 1;
                    break;
                case Index.INDEX4: index = ((int)instructiune.opcode) << 1;
                    break;
                case Index.INDEX5: index = ((int)instructiune.opcode) << 1;
                    break;
                case Index.INDEX6: index = ((int)instructiune.opcode) << 1;
                    break;
                case Index.INDEX7: index = ((int)instructiune.opcode) << 1;
                    break;
                default:
                    break;
            }
        }
        public void SBUS(OpSBUS op)
        {
            switch (op)
            {
                case OpSBUS.NONE:
                    break;
                case OpSBUS.PdRG: sbus = RG[(int)instructiune.rs];
                    break;
                case OpSBUS.PdNRG: sbus = (UInt16)~RG[(int)instructiune.rs];
                    break;
                case OpSBUS.PdT: sbus = T;
                    break;
                case OpSBUS.PdNT: sbus = (UInt16)~T;
                    break;
                case OpSBUS.PdSP: sbus = SP;
                    break;
                case OpSBUS.PdFLAG: sbus = FLAG;
                    break;
                case OpSBUS.PdPC: sbus = PC;
                    break;
                case OpSBUS.PdIVR: sbus = IVR;
                    break;
                case OpSBUS.PdADR: sbus = ADR;
                    break;
                case OpSBUS.PdMDR: sbus = MDR;
                    break;
                case OpSBUS.PdIR: sbus = IR;
                    break;
                case OpSBUS.PdIROffset:
                    {
                        sbyte IROffset = (sbyte)(IR & 0x00ff);
                        sbus = (UInt16)(IROffset);
                    }
                    break;
                case OpSBUS.Pd0: sbus = 0;
                    break;
                case OpSBUS.Pd1: sbus = 1;
                    break;
                case OpSBUS.PdMinus1: sbus = 0xffff;
                    break;
                default:
                    break;
            }
        }
        public void DBUS(OpDBUS op)
        {
            switch (op)
            {
                case OpDBUS.NONE:
                    break;
                case OpDBUS.PdRG: dbus = RG[(int)instructiune.rd];
                    break;
                case OpDBUS.PdNRG: dbus = (UInt16)~RG[(int)instructiune.rd];
                    break;
                case OpDBUS.PdT: dbus = T;
                    break;
                case OpDBUS.PdNT: dbus = (UInt16)~T;
                    break;
                case OpDBUS.PdSP: dbus = SP;
                    break;
                case OpDBUS.PdFLAG: dbus = FLAG;
                    break;
                case OpDBUS.PdPC: dbus = PC;
                    break;
                case OpDBUS.PdIVR: dbus = IVR;
                    break;
                case OpDBUS.PdADR: dbus = ADR;
                    break;
                case OpDBUS.PdMDR: dbus = MDR;
                    break;
                case OpDBUS.PdIR: dbus = IR;
                    break;
                case OpDBUS.Pd0: dbus = 0;
                    break;
                case OpDBUS.Pd1: dbus = 1;
                    break;
                case OpDBUS.PdMinus1: dbus = 0xffff;
                    break;
                default:
                    break;
            }
        }
        internal void SetCond()
        {
            if (rbus == 0)
                tempFLAG |= (ushort)(MASCA_Z);
            else
                tempFLAG &= (ushort)(~MASCA_Z);
            if ((rbus & 0x80) != 0)
                tempFLAG |= (ushort)(MASCA_S);
            else
                tempFLAG &= (ushort)(~MASCA_S);
        }
        public void ALU(OpALU op)
        {
            switch (op)
            {
                case OpALU.NONE:
                    break;
                case OpALU.SBUS: rbus = sbus;
                    break;
                case OpALU.DBUS: rbus = dbus;
                    break;
                case OpALU.NOTDBUS:
                    {
                        rbus = (UInt16)~dbus;
                        SetCond();
                    }
                    break;
                case OpALU.SUM:
                    {
                        try
                        {
                            rbus = (UInt16)checked(sbus + dbus);
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            if (((((sbus & 0x8000) & (dbus & 0x8000)) != 0) && ((rbus & 0x8000) == 0)) ||
                        ((((sbus & 0x8000) & (dbus & 0x8000)) == 0) && ((rbus & 0x8000) != 0)))
                                tempFLAG |= (ushort)(MASCA_V);
                            SetCond();
                        }
                    }
                    break;
                case OpALU.AND:
                    {
                        rbus = (UInt16)(sbus & dbus);
                        SetCond();
                    }
                    break;
                case OpALU.OR:
                    {
                        rbus = (UInt16)(sbus | dbus);
                        SetCond();
                    }
                    break;
                case OpALU.XOR:
                    {
                        rbus = (UInt16)(sbus ^ dbus);
                        SetCond();
                    }
                    break;
                case OpALU.SUB:
                    {
                        try
                        {
                            rbus = (UInt16)checked(sbus - dbus);
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            if (((((sbus & 0x8000) & (dbus & 0x8000)) != 0) && ((rbus & 0x8000) == 0)) ||
                            ((((sbus & 0x8000) & (dbus & 0x8000)) == 0) && ((rbus & 0x8000) != 0)))
                                tempFLAG |= (ushort)(MASCA_V);
                            SetCond();
                        }
                    }
                    break;
                case OpALU.ASL:
                    {
                        if ((dbus & 0x8000) != 0)
                            tempFLAG |= (ushort)(MASCA_C);
                        else
                            tempFLAG &= (ushort)(~MASCA_C);

                        rbus = (UInt16)(dbus << 1);
                        SetCond();
                    }
                    break;
                case OpALU.ASR:
                    {
                        if ((dbus & 0x0001) != 0)
                            tempFLAG |= (ushort)(MASCA_C);
                        else
                            tempFLAG &= (ushort)(~MASCA_C);
                        rbus = (UInt16)((int)dbus >> 1);
                        rbus |= (UInt16)((FLAG & MASCA_C) << 15);
                        SetCond();
                    }
                    break;
                case OpALU.LSR:
                    {
                        rbus = (UInt16)(dbus >> 1);
                        SetCond();
                    }
                    break;
                case OpALU.ROL:
                    {
                        rbus = (UInt16)((dbus << 1) | (dbus >> 15));
                        SetCond();
                    }
                    break;
                case OpALU.ROR:
                    {
                        rbus = (UInt16)((dbus >> 1) | (dbus << 15));
                        SetCond();
                    }
                    break;
                case OpALU.RLC:
                    {
                        rbus = (UInt16)((dbus << 1) | (dbus >> 15));
                        rbus |= (UInt16)((FLAG & MASCA_C) << 0);
                        SetCond();
                    }
                    break;
                case OpALU.RRC:
                    {
                        rbus = (UInt16)((dbus >> 1) | (dbus << 15));
                        rbus |= (UInt16)((FLAG & MASCA_C) << 15);
                        SetCond();
                    }
                    break;
                default:
                    break;
            }
        }
        public void RBUS(OpRBUS op)
        {
            switch (op)
            {
                case OpRBUS.NONE:
                    break;
                case OpRBUS.PmFLAG: FLAG = rbus;
                    break;
                case OpRBUS.PmRG: RG[(int)instructiune.rd] = rbus;
                    break;
                case OpRBUS.PmSP: SP = rbus;
                    break;
                case OpRBUS.PmT: T = rbus;
                    break;
                case OpRBUS.PmPC: PC = rbus;
                    break;
                case OpRBUS.PmIVR: IVR = rbus;
                    break;
                case OpRBUS.PmADR: ADR = rbus;
                    break;
                case OpRBUS.PmMDR: MDR = rbus;
                    break;
                default:
                    break;
            }
        }
        public void MEM(OpMEM op)
        {
            switch (op)
            {
                case OpMEM.NONE:
                    break;
                case OpMEM.IFCH:
                    {
                        IR = Memory[ADR];
                        try
                        {
                            instructiune = new CPUInstructiune(IR);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                    break;
                case OpMEM.READ: MDR = Memory[ADR];
                    break;
                case OpMEM.WRITE: Memory[ADR] = MDR;
                    break;
                default:
                    break;
            }
        }
        public void OTHEROP(OpOTHER op)
        {
            switch (op)
            {
                case OpOTHER.NONE:
                    break;
                case OpOTHER.plus2PC: PC += 1;
                    break;
                case OpOTHER.plus2SP: SP++;
                    break;
                case OpOTHER.minus2SP: SP--;
                    break;
                case OpOTHER.SetBVI: FLAG |= (ushort)(MASCA_BVI);
                    break;
                case OpOTHER.ClearBVI: FLAG &= (ushort)(~MASCA_BVI);
                    break;
                case OpOTHER.ClearC: FLAG &= (ushort)(~MASCA_C);
                    break;
                case OpOTHER.SetC: FLAG |= (ushort)(MASCA_C);
                    break;
                case OpOTHER.ClearV: FLAG &= (ushort)(~MASCA_V);
                    break;
                case OpOTHER.SetV: FLAG |= (ushort)(MASCA_V);
                    break;
                case OpOTHER.ClearZ: FLAG &= (ushort)(~MASCA_Z);
                    break;
                case OpOTHER.SetZ: FLAG |= (ushort)(MASCA_Z);
                    break;
                case OpOTHER.ClearS: FLAG &= (ushort)(~MASCA_S);
                    break;
                case OpOTHER.SetS: FLAG |= (ushort)(MASCA_S);
                    break;
                case OpOTHER.ClearCVZS: FLAG = 0;
                    break;
                case OpOTHER.SetCVZS: FLAG |= (ushort)(MASCA_C | MASCA_S | MASCA_V | MASCA_Z);
                    break;
                case OpOTHER.Cin:
                    break;
                case OpOTHER.SetACLOW: { FLAG |= (ushort)(MASCA_ACLOW); aclow = true; intr = true; }
                    break;
                case OpOTHER.INTA_minus2SP: { SP = (ushort)(SP - 1); intr = false; }
                    break;
                case OpOTHER.PdCond: FLAG = tempFLAG;
                    break;
                default:
                    break;
            }
        }
    }
}
