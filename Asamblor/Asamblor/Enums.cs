﻿namespace Simulator
{
    public enum OpSBUS
    {
        NONE = 0,
        PdRG = 1,
        PdNRG,
        PdT,
        PdNT,
        PdSP,
        PdFLAG,
        PdPC,
        PdIVR,
        PdADR,
        PdMDR,
        PdIR,
        PdIROffset,
        Pd0,
        Pd1,
        PdMinus1
    }
    public enum OpDBUS
    {
        NONE = 0,
        PdRG = 1,
        PdNRG,
        PdT,
        PdNT,
        PdSP,
        PdFLAG,
        PdPC,
        PdIVR,
        PdADR,
        PdMDR,
        PdIR,
        Pd0,
        Pd1,
        PdMinus1
    }
    public enum OpALU
    {
        NONE = 0,
        SBUS = 1,
        DBUS,
        NOTDBUS,
        SUM,
        AND,
        OR,
        XOR,
        SUB,
        ASL,
        ASR,
        LSR,
        ROL,
        ROR,
        RLC,
        RRC
    }
    public enum OpRBUS
    {
        NONE = 0,
        PmFLAG = 1,
        PmRG,
        PmSP,
        PmT,
        PmPC,
        PmIVR,
        PmADR,
        PmMDR
    }
    public enum OpMEM
    {
        NONE = 0,
        IFCH = 1,
        READ,
        WRITE
    }
    public enum OpOTHER
    {
        NONE = 0,
        plus2PC = 1,
        plus2SP,
        minus2SP,
        SetBVI,
        ClearBVI,
        ClearC,
        SetC,
        ClearV,
        SetV,
        ClearZ,
        SetZ,
        ClearS,
        SetS,
        ClearCVZS,
        SetCVZS,
        Cin,
        SetACLOW,
        INTA_minus2SP,
        PdCond
    }
    public enum CondRamif
    {
        STEP = 0,
        JUMPI,
        IF_B1_JUMPI_ELSE_STEP,
        IF_B2_JUMPI_ELSE_STEP,
        IF_B3_JUMPI_ELSE_STEP,
        IF_B4_JUMPI_ELSE_STEP,
        IF_RD_JUMPI_ELSE_STEP,
        IF_C_JUMPI_ELSE_STEP,
        IF_S_JUMPI_ELSE_STEP,
        IF_Z_JUMPI_ELSE_STEP,
        IF_V_JUMPI_ELSE_STEP,
        IF_CIL_JUMPI_ELSE_STEP,
        IF_ACLOW_JUMPI_ELSE_STEP
    }
    public enum CondRamifFalse
    {
        STEP = 0,
        JUMPI,
        IF_NB1_JUMPI_ELSE_STEP,
        IF_NB2_JUMPI_ELSE_STEP,
        IF_NB3_JUMPI_ELSE_STEP,
        IF_NB4_JUMPI_ELSE_STEP,
        IF_NRD_JUMPI_ELSE_STEP,
        IF_NC_JUMPI_ELSE_STEP,
        IF_NS_JUMPI_ELSE_STEP,
        IF_NZ_JUMPI_ELSE_STEP,
        IF_NV_JUMPI_ELSE_STEP,
        IF_INTR_JUMPI_ELSE_STEP
    }
    public enum Index
    {
        INDEX0 = 0, INDEX1, INDEX2, INDEX3, INDEX4, INDEX5, INDEX6, INDEX7
    }
    public enum InstructionClass
    {
        B1, B2, B3, B4
    }
    public enum Registers
    {
        R0, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14, R15
    }
    public enum AddressingMode
    {
        AM, //imediat 
        AD, //registru direct
        AI, //registru indirect
        AX  //indexat
    }
}