﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class Asamblor
    {
        List<String> lines = new List<String>();
        List<String[]> symbols = new List<String[]>();
        List<Instructiune> instructions = new List<Instructiune>();
        Dictionary<String, ushort> opcodes = new Dictionary<string, ushort>();
        Dictionary<String, ushort> registers = new Dictionary<string, ushort>();
        Dictionary<String, ushort> labels = new Dictionary<string, ushort>();
        Dictionary<String, bool> directives = new Dictionary<String, bool>();

        List<int> instructionsPC = new List<int>();
        List<int> linesInstructions = new List<int>();
        List<int> linesPC = new List<int>();
        Dictionary<int, int> linePC = new Dictionary<int, int>();
        ushort[] code;
        String[] separators = { " ", ":", "(", ")", ",", "\t" };
        bool absoluteB2;

        public List<int> LinesPC
        {
            get
            {
                return linesPC;
            }
        }

        /// <summary>
        /// Methods should be called in this order:
        ///   <br></br>  1. InitOpcodes
        ///   <br></br>  2. ParseInstructions
        ///    <br></br> 3. ReplaceLabels
        /// </summary>
        /// <param name="fName">The path to a text file which contains assembly code.</param>
        public Asamblor(String fName, bool absoluteCalls)
        {
            absoluteB2 = absoluteCalls;
            this.ReadFile(fName);
            this.SplitTokens();
        }

        public void InitOpcodes(String file)
        {
            try
            {
                foreach (var line in file.Split('\n'))
                {
                    String s = line;

                    if (s.Trim() != "")
                    {
                        var v = s.Split(' ', '\t');
                        opcodes.Add(v[0], UInt16.Parse(v[1]));
                    }
                }
            }


            catch (Exception)
            {
                throw;
            }
        }
        public void SplitTokens()
        {
            foreach (var item in lines)
            {
                char[] v = { ' ', '\t', ',' };
                this.symbols.Add(item.Split(v, StringSplitOptions.RemoveEmptyEntries));
            }
        }
        /// <summary>
        /// First pass
        /// </summary>
        public void ParseInstructions()
        {
            ushort totalInstrLength = 0;
            foreach (String[] item in symbols)
            {
                
                var instr = new Instructiune(item, this);

                if (instr.instructionLabel != null)
                {
                    labels[instr.instructionLabel] += totalInstrLength;
                }
                if (instr.length > 1)
                    totalInstrLength += (ushort)(instr.length - 1);
                instructions.Add(instr);
            }
        }
        /// <summary>
        /// Second and final pass.
        /// <returns>
        /// <b>returns:</b>
        /// An array with the machine code.
        /// </returns>
        /// </summary>
        /// <param name="startPC">the pc value of the first instruction</param>
        public ushort[] ReplaceLabels(int startPC)
        {
            ushort PC = (ushort)startPC;
            int k = -1;
            foreach (var item in instructions)
            {
                k++;
                if (item.operandLabel != null)
                {
                    ushort adrLabel = GetLabelIndex(item.operandLabel);
                    if (absoluteB2)
                    {
                        if (item.Clasa == InstructionClass.B2)
                            item.IndexMad = (short)(adrLabel + startPC);
                        else
                            item.operandOffset = (short)(adrLabel - PC + startPC - 1);
                    }
                    else
                    {
                        item.operandOffset = (short)(adrLabel - PC + startPC);
                    }
                }
                               
                PC = (ushort)(PC + (item.length));
                instructionsPC.Add(PC-item.length);
                item.PackCode();
            }
            k = 0;
            int prevIns = 0; ;
            for(int i = 0; i < linesInstructions.Count; i++)
            {
                if (prevIns != linesInstructions[i] && linesInstructions[i] != 0)
                {
                    linesPC.Add(instructionsPC[k++]);
                    prevIns = linesInstructions[i];
                }
                else
                    linesPC.Add(-1);
            }

            code = new ushort[PC - startPC];
            int j = 0;
            foreach (var item in instructions)
            {
                var c = item.code;
                for (int i = 0; i < c.Length; i++)
                    code[j++] = c[i];
            }
            return code;
        }
        public void ReadFile(String fName)
        {
            using (StreamReader sr = new StreamReader(fName))
            {
                int lineCount = 0;
                String line = sr.ReadLine();
                while (!sr.EndOfStream)
                {
                    if (line != null)
                    {
                        line = RemoveComments(line);
                        if (line != "")
                        {
                            line = line.Trim();
                            if (line[0] == '.')
                            {
                                bool res;
                                var d = line.Split(' ', '\t')[0];
                                if (directives.TryGetValue(d, out res))
                                    if (!res)
                                        directives[d] = true;
                            }
                            else
                            {
                                lines.Add(line);
                                if (line.Contains(':'))
                                {
                                    var s = line.Split(':')[0];
                                    if (labels.ContainsKey(s))
                                        throw new ApplicationException("Duplicate label (" + s + ") on) line " + lines.Count);
                                    labels.Add(s, (ushort)lines.IndexOf(line));
                                }
                            }

                        }
                    }
                    linesInstructions.Add(lines.Count);
                    lineCount++;
                    line = sr.ReadLine();

                }
            }
        }
        public void WriteFile(String fName)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(fName, FileMode.Create)))
            {
                for (int i = 0; i < code.Length; i++)
                    writer.Write(code[i]);
            }
        }
        public void WriteLabels(String fName)
        {
            using (StreamWriter writer = new StreamWriter(File.Open(fName, FileMode.Create)))
            {
                foreach (var item in labels)
                {
                    writer.WriteLine(item.Key + "\t" + (item.Value).ToString("x"));
                }
            }
        }
        public String RemoveComments(String line)
        {
            line = line.Trim(' ', '\t', '\n');
            if (line.Contains(";"))
            {
                line = line.Remove(line.IndexOf(";"));
            }
            return line;
        }
        public ushort GetOpcode(string p)
        {
            ushort b;
            if (opcodes.TryGetValue(p.ToUpper(), out b))
                return b;
            return 0xffff;
        }
        public ushort GetLabelIndex(string s)
        {
            ushort b;
            s = s.Replace(":", "");
            if (labels.TryGetValue(s, out b))
            {
                return b;
            }
            return 0xffff;
        }
        static public InstructionClass GetInstructionClass(ushort code)
        {
            if (code >= int.Parse(Simulator.Properties.Resources.FirstB1) && code <= int.Parse(Simulator.Properties.Resources.LastB1))
                return InstructionClass.B1;
            if (code >= int.Parse(Simulator.Properties.Resources.FirstB2) && code <= int.Parse(Simulator.Properties.Resources.LastB2))
                return InstructionClass.B2;
            if (code >= int.Parse(Simulator.Properties.Resources.FirstB3) && code <= int.Parse(Simulator.Properties.Resources.LastB3))
                return InstructionClass.B3;
            if (code >= int.Parse(Simulator.Properties.Resources.FirstB4) && code <= int.Parse(Simulator.Properties.Resources.LastB4))
                return InstructionClass.B4;
            throw new ApplicationException("Not a legal opcode!");
        }
    }
}
