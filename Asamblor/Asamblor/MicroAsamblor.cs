﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Simulator
{
    public class MicroInstructiune
    {

        public Int64 code;
        public byte sursaSBUS;
        public byte sursaDBUS;
        public byte opALU;
        public byte destinatieRBUS;
        public byte memOp;
        public byte otherOp;
        public byte condRamif;
        public byte index;
        public byte tf;
        public byte microAdrSalt;
        public String label;
        public void GenerateCode()
        {
            code = 0;           
            code |= microAdrSalt;
            code |= ((Int64)(0x01 & tf) << 8);
            code |= ((Int64)(0x07 & index) << 9);
            code |= ((Int64)(0x0f & condRamif) << 12);
            code |= ((Int64)(0x1f & otherOp) << 16);
            code |= ((Int64)(0x3f & memOp) << 21);
            code |= ((Int64)(0x0f & destinatieRBUS) << 23);
            code |= ((Int64)(0x0f & opALU) << 27);
            code |= ((Int64)(0x0f & sursaDBUS) << 31);
            code |= ((Int64)(0x0f & sursaSBUS) << (35));
        }
    }

    public class MicroAsamblor
    {
        List<String> lines = new List<String>();
        List<MicroInstructiune> instructiuni = new List<MicroInstructiune>();
        public Dictionary<String, byte> sursaDBUS = new Dictionary<string, byte>();
        public Dictionary<String, byte> opALU = new Dictionary<string, byte>();
        public Dictionary<String, byte> destRBUS = new Dictionary<string, byte>();
        public Dictionary<String, byte> memOP = new Dictionary<string, byte>();
        public Dictionary<String, byte> sursaSBUS = new Dictionary<string, byte>();
        public Dictionary<String, byte> otherOperations = new Dictionary<string, byte>();
        public Dictionary<String, byte> condRamifTRUE = new Dictionary<string, byte>();
        public Dictionary<String, byte> condRamifFALSE = new Dictionary<string, byte>();
        public Dictionary<String, byte> index = new Dictionary<string, byte>();
        public Dictionary<String, ushort> labels = new Dictionary<string, ushort>();

        public void ReadComenzi(String fName, Dictionary<String,byte> d)
        {
                int n=0;
                if (int.TryParse(fName.Split('\n')[0], out n))
                    foreach (var line in fName.Split('\n'))
                    {
                    if (n != 0)
                    {

                            String[]  s = line.Split(' ');
                            String c=s[0];
                            for (int j = 1; j < s.Length - 1; j++)
                                c = c +" "+ s[j];
                            byte b =0;
                            if(byte.TryParse(s[s.Length-1],out b))
                                 d.Add(c,b);
                        }
                    
                }
        }
        public void ReadMicrocod(String fName)
        {
            using (StreamReader sr = new StreamReader(fName))
            {
                
                while (!sr.EndOfStream)
                {String line = sr.ReadLine();
                    line = line.Trim();
                    if (line.Contains(":"))
                    {
                        String s = line.Split(':')[0];
                        labels.Add(s,(ushort)(lines.Count));
                        line = line.Remove(0, s.Length);
                        line = line.Replace(": ","");
                    }
                    
                    lines.Add(line);

                    String[] tokens = line.Split(' ');
                    MicroInstructiune mi=new MicroInstructiune(); 
                    mi.sursaSBUS = sursaSBUS[tokens[0]];
                    mi.sursaDBUS = sursaDBUS[tokens[1]];
                    mi.opALU=opALU[tokens[2]];
                    mi.destinatieRBUS=destRBUS[tokens[3]];
                    mi.memOp = memOP[tokens[4]];
                    mi.otherOp = otherOperations[tokens[5]];
                    if (tokens[6].Contains("N"))
                    {
                        mi.condRamif = condRamifFALSE[tokens[6]];
                        mi.tf = 0;//false
                    }
                    else
                    {
                        mi.condRamif = condRamifTRUE[tokens[6]];
                        mi.tf = 1;//true
                    }
                    if (!tokens[6].Trim().Equals("STEP"))
                    {
                        if (tokens[7] != null && tokens[7].Contains("INDEX"))
                        {
                            byte b = 0;
                            if (byte.TryParse(tokens[7].Replace("INDEX", ""), out b))
                                mi.index = b;

                            mi.label = tokens[8];
                        }
                        else
                        {
                            mi.index = 0;
                            if (tokens[7] != "")
                                mi.label = tokens[7];
                        }
                    }
                    instructiuni.Add(mi);
      
                }
            }
        }

        public void ReplaceLabels()
        {
            foreach (var item in instructiuni)
            {
                if (item.label != null)
                    item.microAdrSalt =(byte) labels[item.label];
                item.GenerateCode();
            }
        }
        public Int64[] GetCode()
        {
            Int64[] ret = new Int64[instructiuni.Count+1];
            for (int i = 0; i <instructiuni.Count; i++)
            {
                ret[i] = instructiuni[i].code;
            }
            return ret;
        }
    }
}
