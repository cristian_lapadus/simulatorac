﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulator
{
    public class Instructiune
    {
        Asamblor a;
        InstructionClass clasa;

        public InstructionClass Clasa
        {
            get { return clasa; }
            set { clasa = value; }
        }
        ushort opcode;
        AddressingMode mas, mad;
        Registers rs, rd;
        short indexMas, indexMad;//indecsi sau valori imediate

        public short IndexMas
        {
            get { return indexMas; }
            set { indexMas = value; }
        }

        public short IndexMad
        {
            get { return indexMad; }
            set { indexMad = value; }
        }
        short offset;           //offsetul calculat pt instructiunile din clasa b2 si b3
        String offsetLabel;     //offsetul pt instructiunile din clasa b2 si b3

        String label;           //eticheta "et" din fata instructiunii et:mov r4,r6
        ushort labelIndex;
        ushort len;
        ushort[] machineCode;

        public short operandOffset
        {
            get { return offset; }
            set { offset = value; }
        }
        public String operandLabel
        {
            get { return offsetLabel; }
            set { offsetLabel = value; }
        }
        public String instructionLabel
        {
            get { return this.label; }
        }
        public ushort length
        {
            get { return len; }
        }
        public ushort[] code
        {
            get { return machineCode; }
        }
        public Instructiune(String[] tokens, Asamblor a)
        {
            this.a = a;
            int i = 0;
            if (IsLabel(tokens[i]))
            {
                label = tokens[i++].Replace(":", "");
                labelIndex = a.GetLabelIndex(label);
            }
            opcode = a.GetOpcode(tokens[i++]);
            switch (Asamblor.GetInstructionClass(opcode))
            {
                case InstructionClass.B1:
                    {
                        len = 1;
                        this.clasa = InstructionClass.B1;
                        mad = GetAddressingMode(tokens[i]);
                        if (mad == AddressingMode.AM)
                        {
                            short b;
                            if (Int16.TryParse(tokens[i], out b))
                            {
                                indexMad = b;
                                len++;
                            }
                        }
                        else
                        {
                            rd = GetRegister(tokens[i]);//AI,AD,
                            indexMad = GetIndex(tokens[i]);
                            if (indexMad != 0)
                                len++;
                        }
                        i++;
                        mas = GetAddressingMode(tokens[i]);

                        if (mas == AddressingMode.AM)
                        {
                            short b;
                            if (Int16.TryParse(tokens[i].Trim(), out b))
                            {
                                indexMas = b;
                                len++;
                            }
                        }
                        else
                        {
                            rs = GetRegister(tokens[i]);//AI,AD
                            indexMas = GetIndex(tokens[i]);
                            if (indexMas != 0)
                                len++;
                        }

                    }
                    break;
                case InstructionClass.B2:
                    {
                        this.clasa = InstructionClass.B2;
                        mad = GetAddressingMode(tokens[i]);
                        if (mad == AddressingMode.AM)
                        {
                            ushort result = a.GetLabelIndex(tokens[i]);
                            if (result != 0xffff)
                            {//adresare imediata cu eticheta valida
                                labelIndex = result;
                                offsetLabel = tokens[i];
                            }
                            else
                            {
                                if (tokens[i].StartsWith("0x") &&
                                    ushort.TryParse(tokens[i].Substring(2),
                                    System.Globalization.NumberStyles.AllowHexSpecifier,
                                    null,
                                    out result))
                                {
                                    offset = (sbyte)result;//hex 
                                }
                                if (tokens[i] != null &&
                                    ushort.TryParse(tokens[i],
                                    System.Globalization.NumberStyles.Integer,
                                    null,
                                    out result))
                                {
                                    offset = (short)result;//decimal
                                }
                            }
                            len = 2;
                        }
                        else
                        {
                            rd = GetRegister(tokens[i]);
                            indexMad = GetIndex(tokens[i]);
                            if (indexMad > 0)
                                len = 2;
                            else
                                len = 1;
                        }
                    }
                    break;
                case InstructionClass.B3:
                    {
                        this.clasa = InstructionClass.B3;
                        ushort result = a.GetLabelIndex(tokens[i]);
                        if (result != 0xffff)
                        {//adresare imediata cu eticheta valida
                            labelIndex = result;
                            offsetLabel = tokens[i];
                        }
                        else
                        {//adresare imediata cu valoare explicita
                            if (tokens[i].StartsWith("0x") &&
                                ushort.TryParse(tokens[i].Substring(2),
                                System.Globalization.NumberStyles.AllowHexSpecifier,
                                null,
                                out result))
                            {
                                offset = (sbyte)result;//hex 
                            }
                            if (tokens[i] != null &&
                                ushort.TryParse(tokens[i],
                                System.Globalization.NumberStyles.Integer,
                                null,
                                out result))
                            {
                                offset = (sbyte)result;//decimal
                            }
                        }
                        len = 1;
                    }
                    break;
                case InstructionClass.B4:
                    {
                        this.clasa = InstructionClass.B4;
                        len = 1;
                    }
                    break;
                default:
                    break;
            }
            machineCode = new ushort[len];
        }
        bool IsLabel(String s)
        {
            return (s.EndsWith(":"));
        }
        AddressingMode GetAddressingMode(String token)
        {
            if ((token.Contains("+") || token.Contains("-")) && token.Contains("(") && token.Contains(")"))//adresare indexata  
                return AddressingMode.AX;
            if (token.Contains("(") && token.Contains(")"))//nu e indexata, deci e indirecta
                return AddressingMode.AI;
            foreach (var item in Enum.GetValues(typeof(Registers)))
            {
                if (token.ToUpper() == item.ToString())
                    return AddressingMode.AD;
            }
            short result;

            if (token != null &&
                token.StartsWith("0x") &&
                short.TryParse(token.Substring(2),
                System.Globalization.NumberStyles.AllowHexSpecifier,
                null,
                out result))
            {
                return AddressingMode.AM;//hex 
            }
            if (token != null &&
                short.TryParse(token,
                System.Globalization.NumberStyles.None,
                null,
                out result))
            {
                return AddressingMode.AM;//decimal
            }
            return AddressingMode.AM;//eticheta
        }
        Registers GetRegister(String token)
        {
            if (token.Contains('('))
            {
                token = token.Substring(1, 3);
                if (token.Contains(')'))
                {
                    token = token.Substring(0, token.IndexOf(')'));
                }
            }
            foreach (var item in Enum.GetValues(typeof(Registers)))
            {
                if (token.ToUpper() == item.ToString())
                    return (Registers)item;
            }
            return Registers.R0;
        }
        short GetIndex(String token)
        {
            if (token.Contains("+") || token.Contains("-"))
            {
                token = token.Remove(0, token.IndexOf(')') + 1);
            }
            short result;

            if (token != null &&
                token.StartsWith("0x") &&
                short.TryParse(token.Substring(2),
                System.Globalization.NumberStyles.AllowHexSpecifier,
                null,
                out result))
            {
                return result;//hex 
            }
            if (token != null &&
                short.TryParse(token,
                System.Globalization.NumberStyles.Integer,
                null,
                out result))
            {
                return result;//decimal
            }

            return 0;
        }
        public void PackCode()
        {
            switch (clasa)
            {
                case InstructionClass.B1:
                    {
                        machineCode[0] |= (ushort)(opcode << 12);
                        machineCode[0] |= (ushort)((ushort)mas << 10);
                        machineCode[0] |= (ushort)((ushort)rs << 6);
                        machineCode[0] |= (ushort)((ushort)mad << 4);
                        machineCode[0] |= (ushort)((ushort)rd);
                    }
                    break;
                case InstructionClass.B2:
                    {
                        machineCode[0] |= (ushort)(opcode << 6);
                        machineCode[0] |= (ushort)((ushort)mad << 4);
                        machineCode[0] |= (ushort)((ushort)rd);
                    }
                    break;
                case InstructionClass.B3:
                    {
                        machineCode[0] |= (ushort)(opcode << 8);
                        machineCode[0] |= (ushort)(0x00ff & offset);
                    }
                    break;
                case InstructionClass.B4:
                    {
                        machineCode[0] = opcode;
                    }
                    break;
                default:
                    break;
            }
            if (length > 1)
            {
                switch (clasa)
                {
                    case InstructionClass.B1:
                        {
                            if ((mas == AddressingMode.AX || mas == AddressingMode.AM) && (mad == AddressingMode.AX || mad == AddressingMode.AM))
                            {
                                machineCode[1] |= (ushort)(0xffff & indexMas);
                                machineCode[2] |= (ushort)(0xffff & indexMad);
                            }
                            else if (mas == AddressingMode.AX || mas == AddressingMode.AM)
                            {
                                machineCode[1] |= (ushort)(0xffff & indexMas);
                            }
                            else if (mad == AddressingMode.AX || mad == AddressingMode.AM)
                            {
                                machineCode[1] |= (ushort)(0xffff & indexMad);
                            }
                        }
                        break;
                    case InstructionClass.B2:
                        {
                            machineCode[1] |= (ushort)(0xffff & indexMad);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

}
