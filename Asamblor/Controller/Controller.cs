﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simulator;

namespace Controller
{
    public class SimulatorController
    {
        Asamblor asm;
        MicroAsamblor masm;
        CPU sim;
        UInt16[] cod;
        Int64[] microcod;
        IForm1 view;
        List<int> linesPC;
        int startPC, memSize;

        public List<int> LinesPC
        {
            get
            {
                return linesPC;
            }

            set
            {
                linesPC = value;
            }
        }

        public SimulatorController(IForm1 f)
        {
            startPC = 32;
            memSize = 4096;
            view = f;

        }
        public void Reset()
        {
            if (sim != null)
            {
                sim.Reset();
                view.ResetColor();
                view.UpdateRG();
                view.UpdateADR_IVR();
                view.UpdateMDR_FLAG();
                view.UpdateMem();
            }
        }

        public void InitAsamblor(String filePath)
        {
            asm = new Asamblor(filePath, true);
            asm.InitOpcodes(Simulator.Properties.Resources.opcodes);
            asm.ParseInstructions();
            cod = asm.ReplaceLabels(startPC);
            linesPC = asm.LinesPC;

        }
        public void InitMicroasamblor(String filePath)
        {
            masm = new MicroAsamblor();

            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziDBUS, masm.sursaDBUS);
            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziSBUS, masm.sursaSBUS);
            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziRBUS, masm.destRBUS);
            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziOPALU, masm.opALU);
            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziMEM, masm.memOP);
            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziOTHER, masm.otherOperations);
            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziNRAMIF, masm.condRamifFALSE);
            masm.ReadComenzi(Simulator.Properties.Resources.MicrocomenziRAMIF, masm.condRamifTRUE);

            masm.ReadMicrocod(filePath);
            masm.ReplaceLabels();
            microcod = masm.GetCode();
        }
        public bool InitSimulator()
        {
            if (IsReady())
            {
                sim = new CPU(cod, microcod, startPC, memSize);
                return true;
            }
            return false;
        }
        public bool StepMicroInstructiune()
        {
            if (sim != null)
            {
                sim.StepMicroInstructiune();
                view.MicroInstrText(new string[]{sim.Mar.ToString(),
                                                 sim.MicroInstructiune.s.ToString(),
                                                 sim.MicroInstructiune.d.ToString(),
                                                 sim.MicroInstructiune.a.ToString(),
                                                 sim.MicroInstructiune.r.ToString(),
                                                 sim.MicroInstructiune.m.ToString(),
                                                 sim.MicroInstructiune.o.ToString(),
                                                 sim.MicroInstructiune.C.ToString(),
                                                 sim.MicroInstructiune.i.ToString()}, -1);
                int i = -1;
                if (sim.Instructiune != null)
                {
                    if (sim.Instructiune.mas != AddressingMode.AM)
                        i = (int)sim.Instructiune.rs;
                }
                SBUS(sim.MicroInstructiune.s, i);
                i = -1;
                if (sim.Instructiune != null)
                {
                    if (sim.Instructiune.mad != AddressingMode.AM)
                        i = (int)sim.Instructiune.rd;
                }
                DBUS(sim.MicroInstructiune.d, i);
                OpALU(sim.MicroInstructiune.a);
                i = -1;
                if (sim.Instructiune != null)
                {
                    if (sim.Instructiune.mas != AddressingMode.AM)
                        i = (int)sim.Instructiune.rs;
                    if (sim.Instructiune.mad != AddressingMode.AM)
                        i = (int)sim.Instructiune.rd;
                }
                RBUS(sim.MicroInstructiune.r, i, sim.Rbus);
                MEM(sim.MicroInstructiune.m);
                OTHER(sim.MicroInstructiune.o);
                return true;
            }
            else
                return false;
        }
        public bool StepInstructiune()
        {
            if (sim != null)
            {
                view.ResetColor();
                sim.StepInstructiune();
                view.UpdateRG();
                view.UpdateADR_IVR();
                view.UpdateMDR_FLAG();
                view.UpdateMem();

                return true;
            }
            else
                return false;
        }
        public int StepMicroComanda()
        {
            if (sim != null)
            {

                int res = sim.StepMicroComanda();
                view.MicroInstrText(new string[]{sim.Mar.ToString(),
                                                 sim.MicroInstructiune.s.ToString(),
                                                 sim.MicroInstructiune.d.ToString(),
                                                 sim.MicroInstructiune.a.ToString(),
                                                 sim.MicroInstructiune.r.ToString(),
                                                 sim.MicroInstructiune.m.ToString(),
                                                 sim.MicroInstructiune.o.ToString(),
                                                 sim.MicroInstructiune.C.ToString(),
                                                 sim.MicroInstructiune.i.ToString()}, res);
                switch (res)
                {
                    case 0:
                        {
                            view.ResetColor();
                            int i = -1;
                            if (sim.Instructiune != null)
                            {
                                if (sim.Instructiune.mas != AddressingMode.AM)
                                    i = (int)sim.Instructiune.rs;
                                if (sim.Instructiune.mad != AddressingMode.AM)
                                    i = (int)sim.Instructiune.rd;
                            }
                            SBUS(sim.MicroInstructiune.s, i);
                        }
                        break;
                    case 1:
                        {
                            int i = -1;
                            if (sim.Instructiune != null)
                            {
                                if (sim.Instructiune.mas != AddressingMode.AM)
                                    i = (int)sim.Instructiune.rs;
                                if (sim.Instructiune.mad != AddressingMode.AM)
                                    i = (int)sim.Instructiune.rd;
                            }
                            DBUS(sim.MicroInstructiune.d, i);
                        }
                        break;
                    case 2: OpALU(sim.MicroInstructiune.a);
                        break;
                    case 3:
                        {
                            int i = -1;
                            if (sim.Instructiune != null)
                            {
                                if (sim.Instructiune.mas != AddressingMode.AM)
                                    i = (int)sim.Instructiune.rs;
                                if (sim.Instructiune.mad != AddressingMode.AM)
                                    i = (int)sim.Instructiune.rd;
                            }
                            RBUS(sim.MicroInstructiune.r, i, sim.Rbus);
                        }
                        break;
                    case 4:
                        {
                            MEM(sim.MicroInstructiune.m);
                        }
                        break;
                    case 5:
                        {
                            OTHER(sim.MicroInstructiune.o);
                        }
                        break;
                    case 6:
                        {

                        }
                        break;
                    default:
                        break;
                }
                return 1;
            }
            else
                return -1;
        }

        private void OTHER(OpOTHER opOTHER)
        {
            switch (opOTHER)
            {
                case OpOTHER.NONE:
                    break;
                case OpOTHER.plus2PC: view.Plus2PC();
                    break;
                case OpOTHER.plus2SP: view.Plus2SP();
                    break;
                case OpOTHER.minus2SP: view.Minus2SP();
                    break;
                case OpOTHER.SetBVI: view.OtherFlags();
                    break;
                case OpOTHER.ClearBVI: view.OtherFlags();
                    break;
                case OpOTHER.ClearC: view.OtherFlags();
                    break;
                case OpOTHER.SetC: view.OtherFlags();
                    break;
                case OpOTHER.ClearV: view.OtherFlags();
                    break;
                case OpOTHER.SetV: view.OtherFlags();
                    break;
                case OpOTHER.ClearZ: view.OtherFlags();
                    break;
                case OpOTHER.SetZ: view.OtherFlags();
                    break;
                case OpOTHER.ClearS: view.OtherFlags();
                    break;
                case OpOTHER.SetS: view.OtherFlags();
                    break;
                case OpOTHER.ClearCVZS: view.OtherFlags();
                    break;
                case OpOTHER.SetCVZS: view.OtherFlags();
                    break;
                case OpOTHER.Cin: view.OtherFlags();
                    break;
                case OpOTHER.SetACLOW: view.OtherFlags();
                    break;
                case OpOTHER.INTA_minus2SP: { view.OtherFlags(); view.Minus2SP(); }
                    break;
                case OpOTHER.PdCond: view.OtherFlags();
                    break;
                default:
                    break;
            }
        }

        private void MEM(OpMEM opMEM)
        {
            switch (opMEM)
            {
                case OpMEM.NONE:
                    break;
                case OpMEM.IFCH: view.IFCH(sim.Adr, sim.Ir);
                    break;
                case OpMEM.READ: view.MemRead(sim.Adr, sim.Mdr);
                    break;
                case OpMEM.WRITE: view.MemoryWrite(sim.Adr, sim.Mdr);
                    break;
                default:
                    break;
            }
        }

        private void OpALU(OpALU opALU)
        {
            view.Alu(opALU.ToString().ToUpper());
        }

        private void RBUS(OpRBUS op, int i, ushort value)
        {
            switch (op)
            {
                case OpRBUS.PmFLAG: view.PmFLAG(value);
                    break;
                case OpRBUS.PmRG: view.PmRG(i, (int)value);
                    break;
                case OpRBUS.PmSP: view.PmSP(value);
                    break;
                case OpRBUS.PmT: view.PmT((int)value);
                    break;
                case OpRBUS.PmPC: view.PmPC(value);
                    break;
                case OpRBUS.PmIVR: view.PmIVR(value);
                    break;
                case OpRBUS.PmADR: view.PmADR(value);
                    break;
                case OpRBUS.PmMDR: view.PmMDR((int)value);
                    break;
                default:
                    break;
            }
        }
        private void DBUS(OpDBUS op, int index)
        {
            switch (op)
            {
                case OpDBUS.PdRG: view.PdRGd(index);
                    break;
                case OpDBUS.PdT: view.PdTd();
                    break;
                case OpDBUS.PdSP: view.PdSPd();
                    break;
                case OpDBUS.PdFLAG: view.PdFLAGd();
                    break;
                case OpDBUS.PdPC: view.PdPCd();
                    break;
                case OpDBUS.PdIVR: view.PdIVRd();
                    break;
                case OpDBUS.PdADR: view.PdADRd();
                    break;
                case OpDBUS.PdMDR: view.PdMDRd();
                    break;
                case OpDBUS.PdIR: view.PdIRd();
                    break;
                case OpDBUS.Pd0: view.Pd0d();
                    break;
                case OpDBUS.Pd1: view.Pd1d();
                    break;
                case OpDBUS.PdMinus1: view.PdMinus1d();
                    break;
                default:
                    break;
            }
        }
        public void SBUS(OpSBUS op, int index)
        {
            switch (op)
            {
                case OpSBUS.PdRG: view.PdRGs(index);
                    break;
                case OpSBUS.PdT: view.PdTs();
                    break;
                case OpSBUS.PdSP: view.PdSPs();
                    break;
                case OpSBUS.PdFLAG: view.PdFLAGs();
                    break;
                case OpSBUS.PdPC: view.PdPCs();
                    break;
                case OpSBUS.PdIVR: view.PdIVRs();
                    break;
                case OpSBUS.PdADR: view.PdADRs();
                    break;
                case OpSBUS.PdMDR: view.PdMDRs();
                    break;
                case OpSBUS.PdIR: view.PdIRs();
                    break;
                case OpSBUS.Pd0: view.Pd0s();
                    break;
                case OpSBUS.Pd1: view.Pd1s();
                    break;
                case OpSBUS.PdMinus1: view.PdMinus1s();
                    break;
                default:
                    break;
            }
        }
        public bool IsReady()
        {
            if (masm == null || asm == null || cod == null || microcod == null)
                return false;
            return true;
        }
        public UInt16[] GetRG()
        {
            return sim.GeneralRegisters;
        }
        public UInt16[] GetAdr_Pc_Ivr()
        {
            return new UInt16[] { sim.Adr, sim.Pc, sim.Ivr };
        }
        public UInt16[] GetMdr_Flag()
        {
            return new UInt16[] { sim.Mdr, sim.Ir, sim.T1, sim.Sp, sim.Flag };
        }
        public UInt16[] GetMemory()
        {
            return sim.Mem;
        }
        public UInt16 GetPC()
        {

            return sim.Pc;
        }
        public CPUMicroInstructiune GetMicroInstructiuneaCurenta()
        {
            return sim.MicroInstructiune;
        }



        public bool Run()
        {
            if (sim != null)
            {
                view.ResetColor();
                try
                {
                    sim.Run();
                }
                finally
                {
                    view.UpdateRG();
                    view.UpdateADR_IVR();
                    view.UpdateMDR_FLAG();
                    view.UpdateMem();
                }



                return true;
            }
            else
                return false;
        }



        public int GetMar()
        {
            return (int)sim.Mar;
        }

        public byte[] GetCompiledBytes()
        {
            byte[] ret = new byte[cod.Length * sizeof(UInt16)];
            Buffer.BlockCopy(cod, 0, ret, 0, ret.Length);
            return ret;
        }
    }
}
