﻿using System.Drawing;
namespace Controller
{
    public interface IForm1
    {
        void IFCH(int address,int value);
        void MemoryWrite(int index, int value);
        void MemRead(int address, int value);
        void Pd0d();
        void Pd0s();
        void Pd1d();
        void Pd1s();
        void PdADRd();
        void PdADRs();
        void PdCond(int value);
        void PdFLAGd();
        void PdFLAGs();
        void PdIRd();
        void PdIRs();
        void PdIVRd();
        void PdIVRs();
        void PdMDRd();
        void PdMDRs();
        void PdMinus1d();
        void PdMinus1s();
        void PdPCd();
        void PdPCs();
        void PdRGd(int index);
        void PdRGs(int index);
        void PdSPd();
        void PdSPs();
        void PdTd();
        void PdTs();
        void PmADR(int value);
        void PmFLAG(int value);
        void PmIVR(int value);
        void PmMDR(int value);
        void PmPC(int value);
        void PmRG(int index, int value);
        void PmSP(int value);
        void PmT(int value);
        void Plus2PC();
        void Plus2SP();
        void Minus2SP();
        void OtherFlags();
        void Alu(string s);
        void UpdateADR_IVR();
        void UpdateMDR_FLAG();
        void UpdateMem();
        void UpdateRG();
        void ResetColor();
        void MicroInstrText(string[] text,int n);
    }
}
